package co.mrl.vitco.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.pkmmte.view.CircularImageView;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.mrl.vitco.R;
import co.mrl.vitco.db_tables.APTAttendanceTable;
import co.mrl.vitco.db_tables.AcaGradesTable;
import co.mrl.vitco.db_tables.AttendanceDetailedTable;
import co.mrl.vitco.db_tables.AttendanceMiniTable;
import co.mrl.vitco.db_tables.CoursesTable;
import co.mrl.vitco.db_tables.ImagesTable;
import co.mrl.vitco.db_tables.LinksTable;
import co.mrl.vitco.db_tables.MarksTable;
import co.mrl.vitco.db_tables.MessagesTable;
import co.mrl.vitco.db_tables.OtherDetailsTable;
import co.mrl.vitco.db_tables.SpotlightTable;
import co.mrl.vitco.db_tables.TimeTableTable;
import co.mrl.vitco.db_tables.UpdatedLinksTable;
import io.doorbell.android.Doorbell;

/**
 * Created by Lalit Umbarkar on 19/10/15.
 */

public class MainPageActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String PATH_NAME = "/VITco/Images/";
    @InjectView(R.id.nav_list)
    ListView mDrawerList;
    ActionBarDrawerToggle mDrawerToggle;
    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    //@Optional
    //@InjectView(R.id.profile_dp)
    CircularImageView circular_profile_dp;
    ArrayAdapter<String> mAdapter;
    String mActivityTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page_activity);
        ButterKnife.inject(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Main Page");

        //set_dp_photo();
        mActivityTitle = getTitle().toString();
        addDrawerItems();
        setupDrawer();

        findViewById(R.id.profile_bt).setOnClickListener(this);
        findViewById(R.id.course_page_bt).setOnClickListener(this);
        findViewById(R.id.marks_bt).setOnClickListener(this);
        findViewById(R.id.spotlight_bt).setOnClickListener(this);
        findViewById(R.id.aca_grades_bt).setOnClickListener(this);
        findViewById(R.id.apt_attend_bt).setOnClickListener(this);
        findViewById(R.id.faculty_login_bt).setOnClickListener(this);
        findViewById(R.id.adviser_bt).setOnClickListener(this);
        findViewById(R.id.attendance_bt).setOnClickListener(this);
        findViewById(R.id.timetable_bt).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_bt:
                startActivity(new Intent(MainPageActivity.this, ProfileActivity.class));
                break;
            case R.id.marks_bt:
                startActivity(new Intent(MainPageActivity.this, MarksActivity.class));
                break;
            case R.id.course_page_bt:
                startActivity(new Intent(MainPageActivity.this, AllCoursesListActivity.class));
                break;
            case R.id.spotlight_bt:
                startActivity(new Intent(MainPageActivity.this, SpotlightActivity.class));
                break;
            case R.id.aca_grades_bt:
                startActivity(new Intent(MainPageActivity.this, AcaGradesActivity.class));
                break;
            case R.id.apt_attend_bt:
                startActivity(new Intent(MainPageActivity.this, APTAttendanceActivity.class));
                break;
            case R.id.faculty_login_bt:
                startActivity(new Intent(MainPageActivity.this, FacultyLoginActivity.class));
                break;
            case R.id.adviser_bt:
                startActivity(new Intent(MainPageActivity.this, AdviserDetailsActivity.class));
                break;
            case R.id.attendance_bt:
                startActivity(new Intent(MainPageActivity.this, AttendanceActivity.class));
                break;
            case R.id.timetable_bt:
                startActivity(new Intent(MainPageActivity.this, TimeTableActivity.class));
                break;

        }
    }


    private void set_dp_photo() {

        File root = android.os.Environment.getExternalStorageDirectory();
        File file = new File(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
        if (file.exists()) {
            circular_profile_dp.setSelectorStrokeWidth(10);
            circular_profile_dp.addShadow();
            Bitmap bm = BitmapFactory.decodeFile(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
            bm = Bitmap.createBitmap(bm, 0, 10, bm.getWidth(), bm.getWidth());
            circular_profile_dp.setImageBitmap(bm);
        }
    }

    private void addDrawerItems() {
        String[] osArray = { "TimeTable", "Marks", "SpotLight", "Course Page", "Faculty Login", "Attendance", "APT Attendance", "Academic Grades", "Adviser", "Profile", "Settings"};
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(MainPageActivity.this, TimeTableActivity.class));
                        finish();
                        break;
                    case 1:
                        startActivity(new Intent(MainPageActivity.this, MarksActivity.class));
                        finish();
                        break;
                    case 2:
                        startActivity(new Intent(MainPageActivity.this, SpotlightActivity.class));
                        finish();
                        break;
                    case 3:
                        startActivity(new Intent(MainPageActivity.this, AllCoursesListActivity.class));
                        finish();
                        break;
                    case 4:
                        //startActivity(new Intent(MainPageActivity.this, MainPageActivity.class));
                        //finish();
                        break;
                    case 5:
                        startActivity(new Intent(MainPageActivity.this, AttendanceActivity.class));
                        finish();
                        break;
                    case 6:
                        startActivity(new Intent(MainPageActivity.this, APTAttendanceActivity.class));
                        finish();
                        break;
                    case 7:
                        startActivity(new Intent(MainPageActivity.this, AcaGradesActivity.class));
                        finish();
                        break;
                    case 8:
                        startActivity(new Intent(MainPageActivity.this, AdviserDetailsActivity.class));
                        finish();
                        break;
                    case 9:
                        startActivity(new Intent(MainPageActivity.this, ProfileActivity.class));
                        finish();
                        break;
                    case 10:
                        startActivity(new Intent(MainPageActivity.this, OptionsActivity.class));
                        finish();
                        break;
                }

            }
        });

    }

    private void setupDrawer() {

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("VITco");
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_options:

                startActivity(new Intent(MainPageActivity.this, OptionsActivity.class));
                break;

            case R.id.action_feedback:

                // Calling doorbell
                new Doorbell(MainPageActivity.this, 2159, "apBxZZVQyXmXsqAdTNhpLivrqwsjsjOljvvuu7wYGofddwEvBFEvULqJfshdN0zn").show();
                break;

            case R.id.action_logout:

                SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                SharedPreferences.Editor ed = sp.edit();
                ed.clear();
                ed.apply();

                AcaGradesTable.deleteAll(AcaGradesTable.class);
                APTAttendanceTable.deleteAll(APTAttendanceTable.class);
                AttendanceDetailedTable.deleteAll(AttendanceDetailedTable.class);
                AttendanceMiniTable.deleteAll(AttendanceMiniTable.class);
                CoursesTable.deleteAll(CoursesTable.class);
                ImagesTable.deleteAll(ImagesTable.class);
                LinksTable.deleteAll(LinksTable.class);
                MarksTable.deleteAll(MarksTable.class);
                MessagesTable.deleteAll(MessagesTable.class);
                OtherDetailsTable.deleteAll(OtherDetailsTable.class);
                SpotlightTable.deleteAll(SpotlightTable.class);
                TimeTableTable.deleteAll(TimeTableTable.class);
                UpdatedLinksTable.deleteAll(UpdatedLinksTable.class);

                startActivity(new Intent(MainPageActivity.this, LoginActivity.class));
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }


}
