package co.mrl.vitco.activity;

import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pkmmte.view.CircularImageView;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.mrl.vitco.R;
import co.mrl.vitco.db_tables.APTAttendanceTable;
import co.mrl.vitco.db_tables.AcaGradesTable;
import co.mrl.vitco.db_tables.AttendanceDetailedTable;
import co.mrl.vitco.db_tables.AttendanceMiniTable;
import co.mrl.vitco.db_tables.CoursesTable;
import co.mrl.vitco.db_tables.ImagesTable;
import co.mrl.vitco.db_tables.LinksTable;
import co.mrl.vitco.db_tables.MarksTable;
import co.mrl.vitco.db_tables.MessagesTable;
import co.mrl.vitco.db_tables.OtherDetailsTable;
import co.mrl.vitco.db_tables.SpotlightTable;
import co.mrl.vitco.db_tables.TimeTableTable;
import co.mrl.vitco.db_tables.UpdatedLinksTable;
import co.mrl.vitco.model.CaptchaParser;
import io.doorbell.android.Doorbell;

/**
 * Created by root on 19/10/15.
 */
public class ProfileActivity extends AppCompatActivity {//implements SwipeRefreshLayout.OnRefreshListener{

    public static final String PROFILE_ACCESS_URL = "https://academics.vit.ac.in/student/profile_personal_view.asp";
    public static final String ACCESS_URL = "https://academics.vit.ac.in/student/stud_login.asp";
    public static final String SUBMIT_URL = "https://academics.vit.ac.in/student/stud_login_submit.asp";
    public static final String CAPTCHA_URL = "https://academics.vit.ac.in/student/captcha.asp";
    public static final String PROFILE_DP_URL = "https://academics.vit.ac.in/student/view_photo.asp";
    public static final String PATH_NAME = "/VITco/Images/";
    @InjectView(R.id.nav_list)
    ListView mDrawerList;
    ActionBarDrawerToggle mDrawerToggle;
    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    //@Optional
    //@InjectView(R.id.profile_dp)
    CircularImageView circular_profile_dp;
    ArrayAdapter<String> mAdapter;
    String mActivityTitle;
    Map<String, String> cookies;
    Map<String, String> formFields;
    ProgressDialog pd;
    @InjectView(R.id.main_profile_dp)
    CircularImageView main_profile_dp;
//    SwipeRefreshLayout srl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        ButterKnife.inject(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Profile");

        //set_dp_photo();
        mActivityTitle = getTitle().toString();
        addDrawerItems();
        setupDrawer();

        cookies = new HashMap<>();
        formFields = new HashMap<>();

        /*
        srl = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        srl.setOnRefreshListener(this);
        srl.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        */

        // In case certificate are Expired.
        setTrustAllCerts();
        display_profile(true);
        findViewById(R.id.details_lv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView curr_tv = (TextView) findViewById(R.id.testing_tv);
                if (curr_tv.getLineCount() == curr_tv.getMaxLines()) {
                    ObjectAnimator anim = ObjectAnimator.ofInt(curr_tv, "maxLines", 0);
                    anim.setDuration(500).start();
                } else {
                    ObjectAnimator anim = ObjectAnimator.ofInt(curr_tv, "maxLines", curr_tv.getLineCount());
                    anim.setDuration(500).start();
                }
            }
        });
    }


    /*
    @Override
    public void onRefresh() {
        /**
         * On Refresh called when Screen is Refresh by swiping down the screen.
         * Get the Login details.
         * Do Login
         * Get all course details

        SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
        String reg_no = sp.getString("reg_no", "");
        String password = sp.getString("password", "");
        new AcademicsLogin().execute(reg_no, password);
    }
    */

    public void setTrustAllCerts() {
        /**
         * To Bypass SSL Certificate Check. In case the Academics Portal Certificate is expired.
         */
        try {
            // Create a custom Trust Manager
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };


            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                        public boolean verify(String urlHostName, SSLSession session) {
                            return true;
                        }
                    });
        } catch (Exception e) {
            //We can not recover from this exception.
            //e.printStackTrace();
        }
    }

    public void display_profile(boolean repeat) {


        String[] details_keys = {"Name", "Reg No", "Date of Birth", "Gender", "Native Language",
                "Nationality", "Blood Grp", "VIT Mail", "Hosteler", "Block", "Room",
                "Mess", "ATM No.", "School Name", "Programme", "Street", "Area", "City",
                "Pin", "State", "Country", "Ph No.", "Mobile No.", "Email"};
        Map<String, String> detail_vals = new HashMap<>();

        for (String d_key : details_keys) {
            List<OtherDetailsTable> profile_details = OtherDetailsTable.find(
                    OtherDetailsTable.class, "detailkey = ?", d_key);
            try {
                if (profile_details.get(0).detail_key.equals(d_key)) {
                    detail_vals.put(d_key, profile_details.get(0).detail_value);
                }
            } catch (Exception e) {
                e.printStackTrace();

                if (repeat) {
                    SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                    String reg_no = sp.getString("reg_no", "");
                    String password = sp.getString("password", "");
                    new AcademicsLogin().execute(reg_no, password);
                    pd = ProgressDialog.show(ProfileActivity.this, "", "Loading Profile", true);
                    return;
                }
            }
        }

        TextView tmp_pro = (TextView)findViewById(R.id.profile_name_tv);
        tmp_pro.setText(detail_vals.get("Name"));
        tmp_pro = (TextView)findViewById(R.id.profile_programme_tv);
        tmp_pro.setText(detail_vals.get("Programme"));
        tmp_pro = (TextView)findViewById(R.id.profile_reg_no_tv);
        tmp_pro.setText(detail_vals.get("Reg No"));

        String data = "";
        for (String d_key : detail_vals.keySet()) {
            data += d_key + " " + detail_vals.get(d_key) + " || ";
        }

        // Sugar OR has bugs regarding saving byte array in Database
        //
        //List<ImagesTable> dp_img = ImagesTable.find(ImagesTable.class,"imagedes=?","Profile DP");
        try {

            File root = android.os.Environment.getExternalStorageDirectory();
            File file = new File(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
            if (file.exists()) {
                main_profile_dp.setSelectorStrokeWidth(10);
                main_profile_dp.addShadow();
                Bitmap bm = BitmapFactory.decodeFile(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
                bm = Bitmap.createBitmap(bm, 0, 10, bm.getWidth(), bm.getWidth());
                main_profile_dp.setImageBitmap(bm);
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.i("Profile Image","Image problem");
        }
        TextView test_tv = (TextView) findViewById(R.id.testing_tv);
        test_tv.setText(data);

    }

    public class AcademicsLogin extends AsyncTask<String, String, String> {
        /**
         * Does the Academics Login requires Reg No and Password as arguments
         */

        Bitmap bm;
        Connection.Response resp;
        String cap_val = "";

        @Override
        protected void onPreExecute() {
            /**
             * On Pre is executed before Background. Count Down Timer is scheduled so that
             * If Login is not successful until 1 min. Async task is cancelled.
             */
            super.onPreExecute();
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
            Log.i("Login", " PreExecute");

        }

        @Override
        protected String doInBackground(String... strings) {
            /**
             * Returns  "Success" -> Login successfully.
             *          "Failed" -> UpdatedLinksTable could not be updated successfully.
             *          "Wrong Password" -> When user's Reg No. and Passwords do not match.
             * Does Login for academics.
             * Stores cookies returned in cookies variable.
             */

            String reg_no = strings[0];
            String password = strings[1];

            try {
                Log.i("Login", " 0%");
                resp = Jsoup.connect(ACCESS_URL)
                        .timeout(100000)
                        .method(Connection.Method.GET)
                        .userAgent("Mozilla/5.0").execute();

                // Get request to login form of Academics Page, and store cookies, as well as their form fields.
                // Cookies are important. We will use them everywhere.

                Log.i("Login", "10%");
                cookies = resp.cookies();

                Elements fields = resp.parse().select("form input");
                formFields = new HashMap<>();
                for (Element field : fields) {
                    if (!field.attr("name").equals("")) {
                        formFields.put(field.attr("name"), field.attr("value"));
                    }
                }

                // Post request to Captcha page to retrieve Captcha Image, using cookies.
                Log.i("Login", "20%");

                Connection.Response resultImageResponse = Jsoup.connect(CAPTCHA_URL)
                        .cookies(cookies)
                        .ignoreContentType(true)
                        .method(Connection.Method.GET).timeout(100000).execute();

                Log.i("Login", "30%");
                byte[] bitmap_Data = resultImageResponse.bodyAsBytes();
                bm = BitmapFactory.decodeByteArray(bitmap_Data, 0, bitmap_Data.length);

                // Send this Bitmap to CaptchaParser.java to parse it,
                // It returns captcha value as XXXXXX.

                CaptchaParser cp = new CaptchaParser();
                cp.setBm(bm);
                cap_val = cp.getCaptcha();

                // Enter the details in form fields
                Log.i("Login", "40%");

                formFields.put("regno", reg_no);
                formFields.put("passwd", password);
                formFields.put("vrfcd", cap_val);

                // Get Reg No and Password and make login Login using Post request with cookies.
                Log.i("Login", "40%");

                Connection.Response access_url_resp = Jsoup.connect(SUBMIT_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                                // Extra headers
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Host", "academics.vit.ac.in")
                        .header("Connection", "keep-alive")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_login.asp")
                        .cookies(cookies)
                        .followRedirects(true)
                        .data(formFields)
                        .timeout(100000)
                        .method(Connection.Method.POST).execute();

                // Check if logged in by checking if cookies returned has reg number in it.
                // If Reg No was there, then Academics server accepted Login. Otherwise throw Exception.
                Log.i("Login", "45%");

                boolean logged_in = false;
                Map<String, String> temp = access_url_resp.cookies();
                for (String key : temp.keySet()) {
                    if (key.equals("logstudregno")) {
                        if (temp.get(key).equals(reg_no)) {
                            logged_in = true;
                        }
                    }
                    cookies.put(key, temp.get(key));
                }

                if (logged_in) {
                    SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.clear();
                    editor.putString("reg_no", reg_no);
                    editor.putString("password", password);
                    editor.apply();
                } else {
                    throw new WrongPassword();
                }
                Log.i("Login", "100%");


            } catch (WrongPassword wp) {
                // Password Wrong Caught here.
                //wp.printStackTrace();
                return "Wrong Password";
            } catch (Exception e) {
                // Any Connection Error Caught here.
                //e.printStackTrace();
                return "Network Error";
            }
            return "Success";
        }

        @Override
        protected void onPostExecute(String s) {
            /**
             * Next step, Retrieve all data from all courses.
             */

            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            TextView test_tv = (TextView) findViewById(R.id.testing_tv);
            test_tv.setText("Login " + s);
            switch (s) {
                case "Success":
                    new GetProfileData().execute();
                    break;
                case "Network Error":
//                    srl.setRefreshing(false);
                    pd.dismiss();
                    Toast.makeText(ProfileActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
                    break;
                case "Wrong Password":
//                    srl.setRefreshing(false);
                    pd.dismiss();
                    Toast.makeText(ProfileActivity.this, "Please Check Credentials", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    public class GetProfileData extends AsyncTask<String, String, String> {

        Connection.Response resp;
        Bitmap bm;

        /**
         * Does the extraction of profile data
         *
         * @return "Success" -> Profile data extracted.
         * "Failed" -> Unable to extract data.
         */
        @Override
        protected String doInBackground(String... param) {

            try {
                Map<String, String> detail_map = new HashMap<>();
                Log.i("GET Profile", "10%");


                resp = Jsoup.connect(PROFILE_ACCESS_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                        .cookies(cookies)
                        .method(Connection.Method.GET).execute();

                if (resp.statusCode() == 200) {

                    Log.i("GET Profile", "20%");

                    resp = Jsoup.connect(PROFILE_ACCESS_URL)
                            .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                            .header("Accept-Encoding", "gzip, deflate")
                            .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                            .header("Connection", "keep-alive")
                            .header("Host", "academics.vit.ac.in")
                            .header("DNT", "1")
                            .header("Referer", "https://academics.vit.ac.in/student/stud_menu.asp")
                            .cookies(cookies)
                            .ignoreContentType(false)
                            .timeout(300000).followRedirects(true)
                            .method(Connection.Method.GET).execute();

                    Log.i("GET Profile", "40%");

                    Document doc = resp.parse();
                    Element cur_element = doc.select("table[bordercolor=\"#5A768D\"").first().getAllElements().first();
                    Iterator<Element> data_ele = cur_element.select("td").iterator();
                    //while (data_ele.hasNext()){
                    //    data += data_ele.next().text() + " =-> ";
                    //}

                    data_ele.next(); // Personal Details
                    data_ele.next(); // Name
                    detail_map.put("Name", data_ele.next().text());
                    detail_map.put("Image Url", data_ele.next().attr("src")); // Image : Not Important
                    data_ele.next();
                    detail_map.put("Date of Birth", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("Gender", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("Native Language", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("Nationality", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("Blood Grp", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("VIT Mail", data_ele.next().text());
                    data_ele.next(); // Hosteler
                    String is_hosteller = data_ele.next().text().substring(1);
                    detail_map.put("Hosteler", is_hosteller);
                    if (is_hosteller.equalsIgnoreCase("YES")) { // Yes or No
                        data_ele.next(); // Hostel Addr
                        data_ele.next(); // Block
                        detail_map.put("Block", data_ele.next().text());
                        data_ele.next(); // Room
                        detail_map.put("Room", data_ele.next().text());
                        data_ele.next(); // Mess
                        detail_map.put("Mess", data_ele.next().text());
                    } else {
                        // Not applicable
                        data_ele.next(); // Hostel Addr
                        data_ele.next(); // Block
                        data_ele.next();
                        //data += "Block : " + data_ele.next().text() + " == ";
                        data_ele.next(); // Room
                        data_ele.next();
                        //data += "Room : " + data_ele.next().text() + " == ";
                        data_ele.next(); // Mess
                        data_ele.next();
                        //data += "Mess : " + data_ele.next().text() + " == ";
                    }
                    data_ele.next(); // "School Info"
                    data_ele.next(); // Reg No
                    detail_map.put("Reg No", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("ATM No.", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("School Name", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("Programme", data_ele.next().text());
                    data_ele.next();// Permanent Addr
                    data_ele.next();// Street
                    detail_map.put("Street", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("Area", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("City", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("Pin", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("State", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("Country", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("Ph No.", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("Mobile No.", data_ele.next().text());
                    data_ele.next();
                    detail_map.put("Email", data_ele.next().text());

                    Connection.Response resultImageResponse = Jsoup.connect(PROFILE_DP_URL)
                            .cookies(cookies)
                            .ignoreContentType(true)
                            .method(Connection.Method.GET).timeout(100000).execute();

                    Log.i("Login", "30%");
                    byte[] bitmap_Data = resultImageResponse.bodyAsBytes();

                    File root = android.os.Environment.getExternalStorageDirectory();
                    File dir = new File(root.getAbsolutePath() + PATH_NAME);
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }

                    FileOutputStream fos = new FileOutputStream(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
                    fos.write(bitmap_Data);
                    fos.close();

                    // Sugar ORM having Bugs for saving Byte array in database
                    //ImagesTable add_img = new ImagesTable("Profile DP", bitmap_Data);
                    //add_img.save();

                    // Delete data before inserting new
                    String[] details_keys = {"Name", "Reg No", "Date of Birth", "Gender", "Native Language",
                            "Nationality", "Blood Grp", "VIT Mail", "Hosteler", "Block", "Room",
                            "Mess", "ATM No.", "School Name", "Programme", "Street", "Area", "City",
                            "Pin", "State", "Country", "Ph No.", "Mobile No.", "Email"};
                    for (String d_key : details_keys) {
                        List<OtherDetailsTable> search_new = OtherDetailsTable.find(OtherDetailsTable.class,
                                "detailkey=?", d_key);
                        try {
                            search_new.get(0).delete();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    for (String d_key : detail_map.keySet()) {
                        OtherDetailsTable add_new = new OtherDetailsTable(d_key, detail_map.get(d_key));
                        add_new.save();
                    }

                    return "Success";
                }

                return "Failed";
            } catch (Exception e) {
                e.printStackTrace();
                return "Failed";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("Success")) {
                display_profile(false);
            }
            pd.dismiss();
//            srl.setRefreshing(false);

        }
    }

    public class WrongPassword extends Exception {
        public WrongPassword() {
        }

        @Override
        public String toString() {
            return "Wrong Password Entered";
        }
    }

    private void set_dp_photo() {

        File root = android.os.Environment.getExternalStorageDirectory();
        File file = new File(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
        if (file.exists()) {
            circular_profile_dp.setSelectorStrokeWidth(10);
            circular_profile_dp.addShadow();
            Bitmap bm = BitmapFactory.decodeFile(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
            bm = Bitmap.createBitmap(bm, 0, 10, bm.getWidth(), bm.getWidth());
            circular_profile_dp.setImageBitmap(bm);
        }
    }

    private void addDrawerItems() {
        String[] osArray = { "TimeTable", "Marks", "SpotLight", "Course Page", "Faculty Login", "Attendance", "APT Attendance", "Academic Grades", "Adviser", "Profile", "Settings"};
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(ProfileActivity.this, TimeTableActivity.class));
                        finish();
                        break;
                    case 1:
                        startActivity(new Intent(ProfileActivity.this, MarksActivity.class));
                        finish();
                        break;
                    case 2:
                        startActivity(new Intent(ProfileActivity.this, SpotlightActivity.class));
                        finish();
                        break;
                    case 3:
                        startActivity(new Intent(ProfileActivity.this, AllCoursesListActivity.class));
                        finish();
                        break;
                    case 4:
                        startActivity(new Intent(ProfileActivity.this, MainPageActivity.class));
                        finish();
                        break;
                    case 5:
                        startActivity(new Intent(ProfileActivity.this, AttendanceActivity.class));
                        finish();
                        break;
                    case 6:
                        startActivity(new Intent(ProfileActivity.this, APTAttendanceActivity.class));
                        finish();
                        break;
                    case 7:
                        startActivity(new Intent(ProfileActivity.this, AcaGradesActivity.class));
                        finish();
                        break;
                    case 8:
                        startActivity(new Intent(ProfileActivity.this, AdviserDetailsActivity.class));
                        finish();
                        break;
                    case 9:
                        //startActivity(new Intent(ProfileActivity.this, ProfileActivity.class));
                        //finish();
                        break;
                    case 10:
                        startActivity(new Intent(ProfileActivity.this, OptionsActivity.class));
                        finish();
                        break;
                }

            }
        });

    }

    private void setupDrawer() {

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("VITco");
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_options:

                startActivity(new Intent(ProfileActivity.this, OptionsActivity.class));
                break;

            case R.id.action_feedback:

                // Calling doorbell
                new Doorbell(ProfileActivity.this, 2159, "apBxZZVQyXmXsqAdTNhpLivrqwsjsjOljvvuu7wYGofddwEvBFEvULqJfshdN0zn").show();
                break;

            case R.id.action_logout:

                SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                SharedPreferences.Editor ed = sp.edit();
                ed.clear();
                ed.apply();

                AcaGradesTable.deleteAll(AcaGradesTable.class);
                APTAttendanceTable.deleteAll(APTAttendanceTable.class);
                AttendanceDetailedTable.deleteAll(AttendanceDetailedTable.class);
                AttendanceMiniTable.deleteAll(AttendanceMiniTable.class);
                CoursesTable.deleteAll(CoursesTable.class);
                ImagesTable.deleteAll(ImagesTable.class);
                LinksTable.deleteAll(LinksTable.class);
                MarksTable.deleteAll(MarksTable.class);
                MessagesTable.deleteAll(MessagesTable.class);
                OtherDetailsTable.deleteAll(OtherDetailsTable.class);
                SpotlightTable.deleteAll(SpotlightTable.class);
                TimeTableTable.deleteAll(TimeTableTable.class);
                UpdatedLinksTable.deleteAll(UpdatedLinksTable.class);

                startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

}



