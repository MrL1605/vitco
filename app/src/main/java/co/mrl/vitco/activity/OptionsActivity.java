package co.mrl.vitco.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.mrl.vitco.R;
import io.doorbell.android.Doorbell;

/**
 * Created by lalit on 8/9/15.
 */

@SuppressWarnings("deprecation")
public class OptionsActivity extends AppCompatActivity {

    @InjectView(R.id.feedback_layout)
    RelativeLayout feedback_layout;
    @InjectView(R.id.auto_refresh_cb)
    CheckBox auto_refresh_cb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.options_activity);

        ButterKnife.inject(this);
        setTitle("Options");

        SharedPreferences shared_prefs = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
        String alarm_set = shared_prefs.getString("alarm_set", "");

        if (alarm_set.equals("") || alarm_set.equals("0")) {
            auto_refresh_cb.setChecked(false);
        } else {
            auto_refresh_cb.setChecked(true);
        }


        feedback_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Calling doorbell
                new Doorbell(OptionsActivity.this, 2159, "apBxZZVQyXmXsqAdTNhpLivrqwsjsjOljvvuu7wYGofddwEvBFEvULqJfshdN0zn").show();

            }
        });


        auto_refresh_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                SharedPreferences shared_prefs = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                SharedPreferences.Editor editor = shared_prefs.edit();
                if (b) {
                    editor.putString("alarm_set", "1");
                } else {
                    editor.putString("alarm_set", "0");
                }
                editor.apply();


            }
        });

    }

}
