package co.mrl.vitco.activity;

import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pkmmte.view.CircularImageView;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.mrl.vitco.R;
import co.mrl.vitco.db_tables.APTAttendanceTable;
import co.mrl.vitco.db_tables.AcaGradesTable;
import co.mrl.vitco.db_tables.AttendanceDetailedTable;
import co.mrl.vitco.db_tables.AttendanceMiniTable;
import co.mrl.vitco.db_tables.CoursesTable;
import co.mrl.vitco.db_tables.ImagesTable;
import co.mrl.vitco.db_tables.LinksTable;
import co.mrl.vitco.db_tables.MarksTable;
import co.mrl.vitco.db_tables.MessagesTable;
import co.mrl.vitco.db_tables.OtherDetailsTable;
import co.mrl.vitco.db_tables.SpotlightTable;
import co.mrl.vitco.db_tables.TimeTableTable;
import co.mrl.vitco.db_tables.UpdatedLinksTable;
import co.mrl.vitco.model.CaptchaParser;
import io.doorbell.android.Doorbell;

/**
 * Created by root on 20/10/15.
 */
public class AdviserDetailsActivity extends AppCompatActivity {

    public static final String ADVISER_DETAILS_URL = "https://academics.vit.ac.in/student/faculty_advisor_view.asp";
    public static final String ACCESS_URL = "https://academics.vit.ac.in/student/stud_login.asp";
    public static final String SUBMIT_URL = "https://academics.vit.ac.in/student/stud_login_submit.asp";
    public static final String CAPTCHA_URL = "https://academics.vit.ac.in/student/captcha.asp";
    public static final String ADVISER_PHOTO_URL = "https://academics.vit.ac.in/student/emp_photo.asp";
    public static final String STUDENT_HOME = "https://academics.vit.ac.in/student/stud_menu.asp";
    public static final String PATH_NAME = "/VITco/Images/";
    @InjectView(R.id.nav_list)
    ListView mDrawerList;
    ActionBarDrawerToggle mDrawerToggle;
    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @InjectView(R.id.adviser_iv)
    CircularImageView circularImageView;
    //@Optional
    //@InjectView(R.id.profile_dp)
    CircularImageView circular_profile_dp;
    ArrayAdapter<String> mAdapter;
    String mActivityTitle;
    Map<String, String> cookies;
    Map<String, String> formFields;
    ProgressDialog pd;
    String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.advisior_details_activity);
        ButterKnife.inject(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Adviser Details");

        //set_dp_photo();
        mActivityTitle = getTitle().toString();
        addDrawerItems();
        setupDrawer();

        cookies = new HashMap<>();
        formFields = new HashMap<>();

        /*
        srl = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        srl.setOnRefreshListener(this);
        srl.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        */

        // In case certificate are Expired.
        setTrustAllCerts();
        display_adviser_details(true);

        findViewById(R.id.details_lv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView curr_tv = (TextView) findViewById(R.id.testing_tv);
                if (curr_tv.getLineCount() == curr_tv.getMaxLines()) {
                    ObjectAnimator anim = ObjectAnimator.ofInt(curr_tv, "maxLines", 0);
                    anim.setDuration(500).start();
                } else {
                    ObjectAnimator anim = ObjectAnimator.ofInt(curr_tv, "maxLines", curr_tv.getLineCount());
                    anim.setDuration(500).start();
                }
            }
        });

    }

    public void setTrustAllCerts() {
        /**
         * To Bypass SSL Certificate Check. In case the Academics Portal Certificate is expired.
         */
        try {
            // Create a custom Trust Manager
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };


            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                        public boolean verify(String urlHostName, SSLSession session) {
                            return true;
                        }
                    });
        } catch (Exception e) {
            //We can not recover from this exception.
            //e.printStackTrace();
        }
    }

    public class AcademicsLogin extends AsyncTask<String, String, String> {
        /**
         * Does the Academics Login requires Reg No and Password as arguments
         */

        Bitmap bm;
        Connection.Response resp;
        String cap_val = "";

        @Override
        protected void onPreExecute() {
            /**
             * On Pre is executed before Background. Count Down Timer is scheduled so that
             * If Login is not successful until 1 min. Async task is cancelled.
             */
            super.onPreExecute();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
            Log.i("Login", " PreExecute");

        }

        @Override
        protected String doInBackground(String... strings) {
            /**
             * Returns  "Success" -> Login successfully.
             *          "Failed" -> UpdatedLinksTable could not be updated successfully.
             *          "Wrong Password" -> When user's Reg No. and Passwords do not match.
             * Does Login for academics.
             * Stores cookies returned in cookies variable.
             */

            String reg_no = strings[0];
            String password = strings[1];

            try {
                Log.i("Login", " 0%");
                resp = Jsoup.connect(ACCESS_URL)
                        .timeout(100000)
                        .method(Connection.Method.GET)
                        .userAgent("Mozilla/5.0").execute();

                // Get request to login form of Academics Page, and store cookies, as well as their form fields.
                // Cookies are important. We will use them everywhere.

                Log.i("Login", "10%");
                cookies = resp.cookies();

                Elements fields = resp.parse().select("form input");
                formFields = new HashMap<>();
                for (Element field : fields) {
                    if (!field.attr("name").equals("")) {
                        formFields.put(field.attr("name"), field.attr("value"));
                    }
                }

                // Post request to Captcha page to retrieve Captcha Image, using cookies.
                Log.i("Login", "20%");

                Connection.Response resultImageResponse = Jsoup.connect(CAPTCHA_URL)
                        .cookies(cookies)
                        .ignoreContentType(true)
                        .method(Connection.Method.GET).timeout(100000).execute();

                Log.i("Login", "30%");
                byte[] bitmap_Data = resultImageResponse.bodyAsBytes();
                bm = BitmapFactory.decodeByteArray(bitmap_Data, 0, bitmap_Data.length);

                // Send this Bitmap to CaptchaParser.java to parse it,
                // It returns captcha value as XXXXXX.

                CaptchaParser cp = new CaptchaParser();
                cp.setBm(bm);
                cap_val = cp.getCaptcha();

                // Enter the details in form fields
                Log.i("Login", "40%");

                formFields.put("regno", reg_no);
                formFields.put("passwd", password);
                formFields.put("vrfcd", cap_val);

                // Get Reg No and Password and make login Login using Post request with cookies.
                Log.i("Login", "40%");

                Connection.Response access_url_resp = Jsoup.connect(SUBMIT_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                                // Extra headers
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Host", "academics.vit.ac.in")
                        .header("Connection", "keep-alive")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_login.asp")
                        .cookies(cookies)
                        .followRedirects(true)
                        .data(formFields)
                        .timeout(100000)
                        .method(Connection.Method.POST).execute();

                // Check if logged in by checking if cookies returned has reg number in it.
                // If Reg No was there, then Academics server accepted Login. Otherwise throw Exception.
                Log.i("Login", "45%");

                boolean logged_in = false;
                Map<String, String> temp = access_url_resp.cookies();
                for (String key : temp.keySet()) {
                    if (key.equals("logstudregno")) {
                        if (temp.get(key).equals(reg_no)) {
                            logged_in = true;
                        }
                    }
                    cookies.put(key, temp.get(key));
                }

                if (logged_in) {
                    SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.clear();
                    editor.putString("reg_no", reg_no);
                    editor.putString("password", password);
                    editor.apply();
                } else {
                    throw new WrongPassword();
                }
                Log.i("Login", "100%");


            } catch (WrongPassword wp) {
                // Password Wrong Caught here.
                //wp.printStackTrace();
                return "Wrong Password";
            } catch (Exception e) {
                // Any Connection Error Caught here.
                //e.printStackTrace();
                return "Network Error";
            }
            return "Success";
        }

        @Override
        protected void onPostExecute(String s) {
            /**
             * Next step, Retrieve all data from all courses.
             */

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            TextView test_tv = (TextView) findViewById(R.id.testing_tv);
            test_tv.setText("Login " + s);
            switch (s) {
                case "Success":
                    new GetAdviserData().execute();
                    break;
                case "Network Error":
//                    srl.setRefreshing(false);
                    pd.dismiss();
                    Toast.makeText(AdviserDetailsActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
                    break;
                case "Wrong Password":
//                    srl.setRefreshing(false);
                    pd.dismiss();
                    Toast.makeText(AdviserDetailsActivity.this, "Please Check Credentials", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    public class GetAdviserData extends AsyncTask<String, String, String> {

        Connection.Response resp;
        Bitmap bm;

        /**
         * Does the extraction of Spotlight data
         *
         * @return "Success" -> Profile data extracted.
         * "Failed" -> Unable to extract data.
         */
        @Override
        protected String doInBackground(String... param) {

            try {

                Log.i("GET Profile", "10%");

                resp = Jsoup.connect(ADVISER_DETAILS_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Connection", "keep-alive")
                        .header("Host", "academics.vit.ac.in")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_menu.asp")
                        .cookies(cookies)
                        .ignoreContentType(false)
                        .timeout(100000).followRedirects(true)
                        .method(Connection.Method.GET).execute();

                if (resp.statusCode() == 200) {

                    resp = Jsoup.connect(ADVISER_DETAILS_URL)
                            .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                            .header("Accept-Encoding", "gzip, deflate")
                            .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                            .header("Connection", "keep-alive")
                            .header("Host", "academics.vit.ac.in")
                            .header("DNT", "1")
                            .header("Referer", "https://academics.vit.ac.in/student/stud_menu.asp")
                            .cookies(cookies)
                            .ignoreContentType(false)
                            .timeout(100000).followRedirects(true)
                            .method(Connection.Method.GET).execute();
                    Log.i("GET Profile", "20%");

                    Connection.Response resultImageResponse = Jsoup.connect(ADVISER_PHOTO_URL)
                            .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            .header("Referer", STUDENT_HOME)
                            .cookies(cookies)
                            .ignoreContentType(true)
                            .method(Connection.Method.GET).timeout(100000).execute();

                    Log.i("Login", "30%");
                    byte[] bitmap_Data = resultImageResponse.bodyAsBytes();

                    File root = android.os.Environment.getExternalStorageDirectory();
                    File dir = new File(root.getAbsolutePath() + PATH_NAME);
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }

                    FileOutputStream fos = new FileOutputStream(root.getAbsolutePath() + PATH_NAME + "adviser_dp.bmp");
                    fos.write(bitmap_Data);
                    fos.close();

                    Log.i("Login", "22%");

                } else {
                    return "Failed";
                }

                Log.i("GET Profile", "40%");
                Document doc = resp.parse();
                Element session_legend = doc.select("table[bordercolor=\"#5A768D\"]").first();
                Iterator<Element> all_details = session_legend.select("td[width=\"399\"]").iterator();

                String ad_name = all_details.next().text();
                String ad_desi = all_details.next().text();
                String ad_school = all_details.next().text();
                String ad_phno = all_details.next().text();
                String ad_email = all_details.next().text();
                String ad_room = all_details.next().text();
                String ad_int = all_details.next().text();

                if (ad_name.equals("")) {
                    return "Failed";
                }

                List<OtherDetailsTable> retrieve_data = OtherDetailsTable.find(OtherDetailsTable.class,
                        "detailkey=?", "Adviser Name");
                try {
                    retrieve_data.get(0).delete();
                } catch (Exception ignored) {
                }
                retrieve_data = OtherDetailsTable.find(OtherDetailsTable.class,
                        "detailkey=?", "Adviser Designation");
                try {
                    retrieve_data.get(0).delete();
                } catch (Exception ignored) {
                }
                retrieve_data = OtherDetailsTable.find(OtherDetailsTable.class,
                        "detailkey=?", "Adviser School");
                try {
                    retrieve_data.get(0).delete();
                } catch (Exception ignored) {
                }
                retrieve_data = OtherDetailsTable.find(OtherDetailsTable.class,
                        "detailkey=?", "Adviser PhNo");
                try {
                    retrieve_data.get(0).delete();
                } catch (Exception ignored) {
                }
                retrieve_data = OtherDetailsTable.find(OtherDetailsTable.class,
                        "detailkey=?", "Adviser Email");
                try {
                    retrieve_data.get(0).delete();
                } catch (Exception ignored) {
                }
                retrieve_data = OtherDetailsTable.find(OtherDetailsTable.class,
                        "detailkey=?", "Adviser Room");
                try {
                    retrieve_data.get(0).delete();
                } catch (Exception ignored) {
                }
                retrieve_data = OtherDetailsTable.find(OtherDetailsTable.class,
                        "detailkey=?", "Adviser Intercom");
                try {
                    retrieve_data.get(0).delete();
                } catch (Exception ignored) {
                }
                OtherDetailsTable add_other = new OtherDetailsTable("Adviser Name", ad_name);
                add_other.save();
                add_other = new OtherDetailsTable("Adviser Designation", ad_desi);
                add_other.save();
                add_other = new OtherDetailsTable("Adviser School", ad_school);
                add_other.save();
                add_other = new OtherDetailsTable("Adviser PhNo", ad_phno);
                add_other.save();
                add_other = new OtherDetailsTable("Adviser Email", ad_email);
                add_other.save();
                add_other = new OtherDetailsTable("Adviser Room", ad_room);
                add_other.save();
                add_other = new OtherDetailsTable("Adviser Intercom", ad_int);
                add_other.save();

                return "Success";

            } catch (Exception e) {
                e.printStackTrace();
                return "Failed";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("Success")) {
                display_adviser_details(false);
            }
            pd.dismiss();

//            srl.setRefreshing(false);

        }
    }

    public class WrongPassword extends Exception {
        public WrongPassword() {
        }

        @Override
        public String toString() {
            return "Wrong Password Entered";
        }
    }

    public void display_adviser_details(boolean repeat) {

        data = "";

        List<OtherDetailsTable> retrieve_data = OtherDetailsTable.find(OtherDetailsTable.class,
                "detailkey=?", "Adviser Name");
        TextView all_tmp;
        if (retrieve_data.size() <= 0 && repeat) {

            SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
            String reg_no = sp.getString("reg_no", "");
            String password = sp.getString("password", "");
            new AcademicsLogin().execute(reg_no, password);
            pd = ProgressDialog.show(AdviserDetailsActivity.this, "", "Loading Adviser Details", true);
            return;
        }

        try {
            data += retrieve_data.get(0).detail_key + " " + retrieve_data.get(0).detail_value;
            all_tmp = (TextView) findViewById(R.id.adviser_name_tv);
            all_tmp.setText(retrieve_data.get(0).detail_value);
        } catch (Exception ignored) {
        }
        retrieve_data = OtherDetailsTable.find(OtherDetailsTable.class,
                "detailkey=?", "Adviser Designation");
        try {
            data += retrieve_data.get(0).detail_key + " " + retrieve_data.get(0).detail_value;
            all_tmp = (TextView) findViewById(R.id.adviser_designation_tv);
            all_tmp.setText(retrieve_data.get(0).detail_value);
        } catch (Exception ignored) {
        }
        retrieve_data = OtherDetailsTable.find(OtherDetailsTable.class,
                "detailkey=?", "Adviser School");
        try {
            data += retrieve_data.get(0).detail_key + " " + retrieve_data.get(0).detail_value;
            all_tmp = (TextView) findViewById(R.id.adviser_school_tv);
            all_tmp.setText(retrieve_data.get(0).detail_value);
        } catch (Exception ignored) {
        }
        retrieve_data = OtherDetailsTable.find(OtherDetailsTable.class,
                "detailkey=?", "Adviser PhNo");
        try {
            data += retrieve_data.get(0).detail_key + " " + retrieve_data.get(0).detail_value;
        } catch (Exception ignored) {
        }
        retrieve_data = OtherDetailsTable.find(OtherDetailsTable.class,
                "detailkey=?", "Adviser Email");
        try {
            data += retrieve_data.get(0).detail_key + " " + retrieve_data.get(0).detail_value;
        } catch (Exception ignored) {
        }
        retrieve_data = OtherDetailsTable.find(OtherDetailsTable.class,
                "detailkey=?", "Adviser Room");
        try {
            data += retrieve_data.get(0).detail_key + " " + retrieve_data.get(0).detail_value;
        } catch (Exception ignored) {
        }
        retrieve_data = OtherDetailsTable.find(OtherDetailsTable.class,
                "detailkey=?", "Adviser Intercom");
        try {
            data += retrieve_data.get(0).detail_key + " " + retrieve_data.get(0).detail_value;
        } catch (Exception ignored) {
        }

        circularImageView.setSelectorStrokeWidth(10);
        circularImageView.addShadow();

        File root = android.os.Environment.getExternalStorageDirectory();
        File file = new File(root.getAbsolutePath() + PATH_NAME + "adviser_dp.bmp");
        if (file.exists()) {
            Bitmap bm = BitmapFactory.decodeFile(root.getAbsolutePath() + PATH_NAME + "adviser_dp.bmp");
            bm = Bitmap.createBitmap(bm, 0, 10, bm.getWidth(), bm.getWidth());
            circularImageView.setImageBitmap(bm);
        }

        TextView test_tv = (TextView) findViewById(R.id.testing_tv);
        test_tv.setText(data);

    }


    private void set_dp_photo() {

        File root = android.os.Environment.getExternalStorageDirectory();
        File file = new File(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
        if (file.exists()) {
            circular_profile_dp.setSelectorStrokeWidth(10);
            circular_profile_dp.addShadow();
            Bitmap bm = BitmapFactory.decodeFile(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
            bm = Bitmap.createBitmap(bm, 0, 10, bm.getWidth(), bm.getWidth());
            circular_profile_dp.setImageBitmap(bm);
        }
    }

    private void addDrawerItems() {
        String[] osArray = {"TimeTable", "Marks", "SpotLight", "Course Page", "Faculty Login", "Attendance", "APT Attendance", "Academic Grades", "Adviser", "Profile", "Settings"};
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(AdviserDetailsActivity.this, TimeTableActivity.class));
                        finish();
                        break;
                    case 1:
                        startActivity(new Intent(AdviserDetailsActivity.this, MarksActivity.class));
                        finish();
                        break;
                    case 2:
                        startActivity(new Intent(AdviserDetailsActivity.this, SpotlightActivity.class));
                        finish();
                        break;
                    case 3:
                        startActivity(new Intent(AdviserDetailsActivity.this, AllCoursesListActivity.class));
                        finish();
                        break;
                    case 4:
                        startActivity(new Intent(AdviserDetailsActivity.this, MainPageActivity.class));
                        finish();
                        break;
                    case 5:
                        startActivity(new Intent(AdviserDetailsActivity.this, AttendanceActivity.class));
                        finish();
                        break;
                    case 6:
                        startActivity(new Intent(AdviserDetailsActivity.this, APTAttendanceActivity.class));
                        finish();
                        break;
                    case 7:
                        startActivity(new Intent(AdviserDetailsActivity.this, AcaGradesActivity.class));
                        finish();
                        break;
                    case 8:
                        //startActivity(new Intent(AdviserDetailsActivity.this, AdviserDetailsActivity.class));
                        //finish();
                        break;
                    case 9:
                        startActivity(new Intent(AdviserDetailsActivity.this, ProfileActivity.class));
                        finish();
                        break;
                    case 10:
                        startActivity(new Intent(AdviserDetailsActivity.this, OptionsActivity.class));
                        finish();
                        break;
                }

            }
        });

    }

    private void setupDrawer() {

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("VITco");
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_options:

                startActivity(new Intent(AdviserDetailsActivity.this, OptionsActivity.class));
                break;

            case R.id.action_feedback:

                // Calling doorbell
                new Doorbell(AdviserDetailsActivity.this, 2159, "apBxZZVQyXmXsqAdTNhpLivrqwsjsjOljvvuu7wYGofddwEvBFEvULqJfshdN0zn").show();
                break;

            case R.id.action_logout:

                SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                SharedPreferences.Editor ed = sp.edit();
                ed.clear();
                ed.apply();

                AcaGradesTable.deleteAll(AcaGradesTable.class);
                APTAttendanceTable.deleteAll(APTAttendanceTable.class);
                AttendanceDetailedTable.deleteAll(AttendanceDetailedTable.class);
                AttendanceMiniTable.deleteAll(AttendanceMiniTable.class);
                CoursesTable.deleteAll(CoursesTable.class);
                ImagesTable.deleteAll(ImagesTable.class);
                LinksTable.deleteAll(LinksTable.class);
                MarksTable.deleteAll(MarksTable.class);
                MessagesTable.deleteAll(MessagesTable.class);
                OtherDetailsTable.deleteAll(OtherDetailsTable.class);
                SpotlightTable.deleteAll(SpotlightTable.class);
                TimeTableTable.deleteAll(TimeTableTable.class);
                UpdatedLinksTable.deleteAll(UpdatedLinksTable.class);

                startActivity(new Intent(AdviserDetailsActivity.this, LoginActivity.class));
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }


}
