package co.mrl.vitco.activity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import co.mrl.vitco.R;
import co.mrl.vitco.db_tables.AttendanceDetailedTable;
import co.mrl.vitco.db_tables.AttendanceMiniTable;
import co.mrl.vitco.db_tables.CoursesTable;
import co.mrl.vitco.model.CaptchaParser;

/**
 * Created by Lalit Umbarkar on 25/10/15.
 */
public class AttendanceDetailActivity extends AppCompatActivity {

    public static final String ATTENDANCE_URL = "https://academics.vit.ac.in/student/attn_report.asp";
    public static final String ATTENDANCE_DETAIL_URL = "https://academics.vit.ac.in/student/attn_report_details.asp";
    public static final String ACCESS_URL = "https://academics.vit.ac.in/student/stud_login.asp";
    public static final String SUBMIT_URL = "https://academics.vit.ac.in/student/stud_login_submit.asp";
    public static final String CAPTCHA_URL = "https://academics.vit.ac.in/student/captcha.asp";
    Map<String, String> cookies;
    Map<String, String> formFields;
    ProgressDialog pd;
    SwipeRefreshLayout srl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailed_attendance);

        String class_nbr = getIntent().getExtras().getString("class_nbr");

        srl = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pd = ProgressDialog.show(AttendanceDetailActivity.this, "", "Loading Attendance details", true);
                SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                String reg_no = sp.getString("reg_no", "");
                String password = sp.getString("password", "");
                new AcademicsLogin().execute(reg_no, password);
            }
        });
        srl.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        List<CoursesTable> temp_course = CoursesTable.find(CoursesTable.class, "classnbr=?", class_nbr);
        setTitle(temp_course.get(0).course_name);

        show_data(class_nbr);

    }

    private void show_data(String class_nbr) {

        List<AttendanceDetailedTable> course_details = AttendanceDetailedTable.find(
                AttendanceDetailedTable.class, "classnbr=?", class_nbr);

        String[] tmp_slno = new String[100];
        String[] tmp_units = new String[100];
        String[] tmp_status = new String[100];
        String[] tmp_date = new String[100];
        String[] tmp_reason = new String[100];

        int i = 0;
        for (AttendanceDetailedTable temp : course_details) {
            Log.i("class date","" + temp.date);
            tmp_slno[i] = temp.slno;
            tmp_date[i] = temp.date;
            tmp_units[i] = temp.units;
            tmp_status[i] = temp.status;
            tmp_reason[i] = temp.reason;
            i += 1;
        }

        String[] att_date = new String[i];
        String[] att_slno = new String[i];
        String[] att_unit = new String[i];
        String[] att_reason = new String[i];
        String[] att_status = new String[i];

        for (int j = 0; j < i; j++) {

            att_slno[j] = tmp_slno[j];
            att_date[j] = tmp_date[j];
            att_unit[j] = tmp_units[j];
            att_reason[j] = tmp_reason[j];
            att_status[j] = tmp_status[j];
        }


        // Sort the courses by their names.
        for (int j = 0; j < att_date.length; j++) {
            for (i = j + 1; i < att_date.length; i++) {
                if (att_slno[i].compareToIgnoreCase(att_slno[j]) < 0) {
                    String temp = att_date[j];
                    String temp_slno = att_slno[j];
                    String temp_unit = att_unit[j];
                    String temp_stat = att_status[j];
                    String temp_reas = att_reason[j];
                    att_date[j] = att_date[i];
                    att_unit[j] = att_unit[i];
                    att_status[j] = att_status[i];
                    att_reason[j] = att_reason[i];
                    att_slno[j] = att_slno[i];
                    att_date[i] = temp;
                    att_slno[i] = temp_slno;
                    att_unit[i] = temp_unit;
                    att_reason[i] = temp_reas;
                    att_status[i] = temp_stat;
                }
            }
        }


        GridLayout tmp_grid = (GridLayout)findViewById(R.id.detailed_gl);

        TextView date_tv = new TextView(this);
        date_tv.setText("Class Date");
        TextView status_tv = new TextView(this);
        status_tv.setText("Status");
        TextView units_tv = new TextView(this);
        units_tv.setText("Units");
        TextView reason_tv = new TextView(this);
        reason_tv.setText("Reason");
        tmp_grid.addView(date_tv);
        tmp_grid.addView(status_tv);
        tmp_grid.addView(units_tv);
        tmp_grid.addView(reason_tv);

        for(int ind =0; ind < att_date.length; ind++){
            date_tv = new TextView(this);
            date_tv.setText(att_date[ind]);
            status_tv = new TextView(this);
            status_tv.setText(att_status[ind]);
            units_tv = new TextView(this);
            units_tv.setText(att_unit[ind]);
            reason_tv = new TextView(this);
            reason_tv.setText(att_reason[ind]);
            tmp_grid.addView(date_tv);
            tmp_grid.addView(status_tv);
            tmp_grid.addView(units_tv);
            tmp_grid.addView(reason_tv);
        }
    }


    public void setTrustAllCerts() {
        /**
         * To Bypass SSL Certificate Check. In case the Academics Portal Certificate is expired.
         */
        try {
            // Create a custom Trust Manager
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };


            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                        public boolean verify(String urlHostName, SSLSession session) {
                            return true;
                        }
                    });
        } catch (Exception e) {
            //We can not recover from this exception.
            //e.printStackTrace();
        }
    }

    public class AcademicsLogin extends AsyncTask<String, String, String> {
        /**
         * Does the Academics Login requires Reg No and Password as arguments
         */

        Bitmap bm;
        Connection.Response resp;
        String cap_val = "";

        @Override
        protected void onPreExecute() {
            /**
             * On Pre is executed before Background. Count Down Timer is scheduled so that
             * If Login is not successful until 1 min. Async task is cancelled.
             */
            super.onPreExecute();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
            Log.i("Login", " PreExecute");

        }

        @Override
        protected String doInBackground(String... strings) {
            /**
             * Returns  "Success" -> Login successfully.
             *          "Failed" -> UpdatedLinksTable could not be updated successfully.
             *          "Wrong Password" -> When user's Reg No. and Passwords do not match.
             * Does Login for academics.
             * Stores cookies returned in cookies variable.
             */

            String reg_no = strings[0];
            String password = strings[1];

            try {
                Log.i("Login", " 0%");
                resp = Jsoup.connect(ACCESS_URL)
                        .timeout(100000)
                        .method(Connection.Method.GET)
                        .userAgent("Mozilla/5.0").execute();

                // Get request to login form of Academics Page, and store cookies, as well as their form fields.
                // Cookies are important. We will use them everywhere.

                Log.i("Login", "10%");
                cookies = resp.cookies();

                Elements fields = resp.parse().select("form input");
                formFields = new HashMap<>();
                for (Element field : fields) {
                    if (!field.attr("name").equals("")) {
                        formFields.put(field.attr("name"), field.attr("value"));
                    }
                }

                // Post request to Captcha page to retrieve Captcha Image, using cookies.
                Log.i("Login", "20%");

                Connection.Response resultImageResponse = Jsoup.connect(CAPTCHA_URL)
                        .cookies(cookies)
                        .ignoreContentType(true)
                        .method(Connection.Method.GET).timeout(100000).execute();

                Log.i("Login", "30%");
                byte[] bitmap_Data = resultImageResponse.bodyAsBytes();
                bm = BitmapFactory.decodeByteArray(bitmap_Data, 0, bitmap_Data.length);

                // Send this Bitmap to CaptchaParser.java to parse it,
                // It returns captcha value as XXXXXX.

                CaptchaParser cp = new CaptchaParser();
                cp.setBm(bm);
                cap_val = cp.getCaptcha();

                // Enter the details in form fields
                Log.i("Login", "40%");

                formFields.put("regno", reg_no);
                formFields.put("passwd", password);
                formFields.put("vrfcd", cap_val);

                // Get Reg No and Password and make login Login using Post request with cookies.
                Log.i("Login", "40%");

                Connection.Response access_url_resp = Jsoup.connect(SUBMIT_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                                // Extra headers
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Host", "academics.vit.ac.in")
                        .header("Connection", "keep-alive")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_login.asp")
                        .cookies(cookies)
                        .followRedirects(true)
                        .data(formFields)
                        .timeout(100000)
                        .method(Connection.Method.POST).execute();

                // Check if logged in by checking if cookies returned has reg number in it.
                // If Reg No was there, then Academics server accepted Login. Otherwise throw Exception.
                Log.i("Login", "45%");

                boolean logged_in = false;
                Map<String, String> temp = access_url_resp.cookies();
                for (String key : temp.keySet()) {
                    if (key.equals("logstudregno")) {
                        if (temp.get(key).equals(reg_no)) {
                            logged_in = true;
                        }
                    }
                    cookies.put(key, temp.get(key));
                }

                if (logged_in) {
                    SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.clear();
                    editor.putString("reg_no", reg_no);
                    editor.putString("password", password);
                    editor.apply();
                } else {
                    throw new WrongPassword();
                }
                Log.i("Login", "100%");


            } catch (WrongPassword wp) {
                // Password Wrong Caught here.
                //wp.printStackTrace();
                return "Wrong Password";
            } catch (Exception e) {
                // Any Connection Error Caught here.
                //e.printStackTrace();
                return "Network Error";
            }
            return "Success";
        }

        @Override
        protected void onPostExecute(String s) {
            /**
             * Next step, Retrieve all data from all courses.
             */

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            switch (s) {
                case "Success":
                    new GetAttendanceData().execute();
                    break;
                case "Network Error":
                    srl.setRefreshing(false);
                    pd.dismiss();
                    Toast.makeText(AttendanceDetailActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
                    break;
                case "Wrong Password":
                    srl.setRefreshing(false);
                    pd.dismiss();
                    Toast.makeText(AttendanceDetailActivity.this, "Please Check Credentials", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    public class GetAttendanceData extends AsyncTask<String, String, String> {

        Connection.Response resp;

        /**
         * Does the extraction of Spotlight data
         *
         * @return "Success" -> Profile data extracted.
         * "Failed" -> Unable to extract data.
         */
        @Override
        protected String doInBackground(String... param) {

            try {

                Log.i("GET Profile", "10%");

                resp = Jsoup.connect(ATTENDANCE_URL + "?sem=FS&fmdt=&todt=")
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Connection", "keep-alive")
                        .header("Host", "academics.vit.ac.in")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_menu.asp")
                        .cookies(cookies)
                        .ignoreContentType(false)
                        .timeout(100000).followRedirects(true)
                        .method(Connection.Method.GET).execute();

                if (resp.statusCode() == 200) {

                    resp = Jsoup.connect(ATTENDANCE_URL + "?sem=FS&fmdt=&todt=")
                            .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                            .header("Accept-Encoding", "gzip, deflate")
                            .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                            .header("Connection", "keep-alive")
                            .header("Host", "academics.vit.ac.in")
                            .header("DNT", "1")
                            .header("Referer", "https://academics.vit.ac.in/student/stud_menu.asp")
                            .cookies(cookies)
                            .ignoreContentType(false)
                            .timeout(100000).followRedirects(true)
                            .method(Connection.Method.GET).execute();
                    Log.i("GET Profile", "20%");
                } else {
                    return "Failed";
                }


                Document doc = resp.parse();
                Elements fields = doc.select("form input");
                formFields = new HashMap<>();
                for (Element field : fields) {
                    if (!field.attr("name").equals("")) {
                        formFields.put(field.attr("name"), field.attr("value"));
                        Log.i("input fields:", "added " + field.attr("name") + " " + field.attr("value"));
                    }
                }


                String frm_date = formFields.get("from_date");
                String to_date;

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
                to_date = df.format(cal.getTime());

                Log.i("Attendance", frm_date + to_date);

                resp = Jsoup.connect(ATTENDANCE_URL + "?sem=FS&fmdt=" + frm_date + "&todt=" + to_date)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Connection", "keep-alive")
                        .header("Host", "academics.vit.ac.in")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_menu.asp")
                        .cookies(cookies)
                        .ignoreContentType(false)
                        .timeout(100000).followRedirects(true)
                        .method(Connection.Method.GET).execute();

                Log.i("GET Profile", "40%");
                doc = resp.parse();
                Element att_table = doc.select("table[width=\"900\"]").first();
                Elements all_details = att_table.select("tr[bgcolor=\"#E6F2FF\"]");

                // Delete all if more than 1 row found
                if (all_details.size() > 0) {
                    AttendanceMiniTable.deleteAll(AttendanceMiniTable.class);
                } else {
                    return "Failed";
                }

                for (Element row : all_details) {
                    formFields = new HashMap<>();
                    Iterator<Element> col = row.select("td").iterator();

                    col.next().text(); // Sl No
                    col.next().text(); // Course Code
                    col.next().text(); // Course Title
                    col.next().text(); // Course Type
                    col.next().text(); // Course Slot
                    col.next().text(); // Course Reg Date
                    String attend_classes = col.next().text();
                    String total_classes = col.next().text();
                    String att_per = col.next().text();
                    String deb_status = col.next().text();
                    Element view_bt = col.next();
                    String classnbr = view_bt.select("input[name=\"classnbr\"]").first().val();
                    for (Element button : view_bt.select("input")) {
                        //data += button.attr("name") + " => " + button.val();
                        if (!button.attr("name").equals("")) {
                            formFields.put(button.attr("name"), button.val());
                            Log.i("attend", button.attr("name") + " => " + button.val());
                        }
                    }

                    AttendanceMiniTable att_mini_table = new AttendanceMiniTable(
                            Integer.parseInt(classnbr), Integer.parseInt(attend_classes),
                            Integer.parseInt(total_classes), att_per, deb_status);
                    att_mini_table.save();

                    Document detail_doc = Jsoup.connect(ATTENDANCE_DETAIL_URL)
                            .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                            .header("Accept-Encoding", "gzip, deflate")
                            .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                            .header("Connection", "keep-alive")
                            .header("Host", "academics.vit.ac.in")
                            .header("DNT", "1")
                            .header("Referer", "https://academics.vit.ac.in/student/stud_menu.asp")
                            .cookies(cookies)
                            .data(formFields)
                            .ignoreContentType(false)
                            .timeout(100000).followRedirects(true)
                            .method(Connection.Method.POST).execute().parse();

                    Elements detail_rows = detail_doc.select("table[cellpadding=\"4\"]").get(1).select("tr");

                    // Check if rows are there
                    if (detail_rows.size() > 0) {
                        // If more than 1 delete data
                        List<AttendanceDetailedTable> del_detailed = AttendanceDetailedTable.find(AttendanceDetailedTable.class,
                                "classnbr=?", classnbr);
                        for (AttendanceDetailedTable curr_ : del_detailed) {
                            curr_.delete();
                        }

                        // Store left out data.
                        for (Element detail_row : detail_rows) {
                            if (!detail_row.attr("bgcolor").equals("#5A768D")) {
                                Iterator<Element> det_col = detail_row.select("td").iterator();
                                String slno = det_col.next().text(); // Sl No
                                String date = det_col.next().text();
                                String slot = det_col.next().text(); // Slot
                                String class_status = det_col.next().text();
                                String class_unit = det_col.next().text();
                                String reason = det_col.next().text();
                                AttendanceDetailedTable add_detail = new AttendanceDetailedTable(
                                        slno, date, slot, class_status, class_unit, reason,
                                        Integer.parseInt(classnbr));
                                add_detail.save();
                            }
                        }
                    }
                    Log.i("detail", "completed");
                }

//                */
                return "Success";

            } catch (Exception e) {
                e.printStackTrace();
                return "Failed";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("Success")) {
                //display_detailed_marks(false);
            }
            pd.dismiss();
            srl.setRefreshing(false);
        }
    }

    public class WrongPassword extends Exception {
        public WrongPassword() {
        }

        @Override
        public String toString() {
            return "Wrong Password Entered";
        }
    }


}
