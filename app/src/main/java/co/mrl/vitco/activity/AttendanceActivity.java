package co.mrl.vitco.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.pkmmte.view.CircularImageView;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.mrl.vitco.R;
import co.mrl.vitco.adapter.AttendanceListAdapter;
import co.mrl.vitco.db_tables.APTAttendanceTable;
import co.mrl.vitco.db_tables.AcaGradesTable;
import co.mrl.vitco.db_tables.AttendanceDetailedTable;
import co.mrl.vitco.db_tables.AttendanceMiniTable;
import co.mrl.vitco.db_tables.CoursesTable;
import co.mrl.vitco.db_tables.ImagesTable;
import co.mrl.vitco.db_tables.LinksTable;
import co.mrl.vitco.db_tables.MarksTable;
import co.mrl.vitco.db_tables.MessagesTable;
import co.mrl.vitco.db_tables.OtherDetailsTable;
import co.mrl.vitco.db_tables.SpotlightTable;
import co.mrl.vitco.db_tables.TimeTableTable;
import co.mrl.vitco.db_tables.UpdatedLinksTable;
import co.mrl.vitco.model.CaptchaParser;
import io.doorbell.android.Doorbell;

/**
 * Created by Lalit Umbarkar on 20/10/15.
 */
public class AttendanceActivity extends AppCompatActivity {

    public static final String ATTENDANCE_URL = "https://academics.vit.ac.in/student/attn_report.asp";
    public static final String ATTENDANCE_DETAIL_URL = "https://academics.vit.ac.in/student/attn_report_details.asp";
    public static final String ACCESS_URL = "https://academics.vit.ac.in/student/stud_login.asp";
    public static final String SUBMIT_URL = "https://academics.vit.ac.in/student/stud_login_submit.asp";
    public static final String CAPTCHA_URL = "https://academics.vit.ac.in/student/captcha.asp";
    public static final String PATH_NAME = "/VITco/Images/";
    @InjectView(R.id.nav_list)
    ListView mDrawerList;
    ActionBarDrawerToggle mDrawerToggle;
    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    //@Optional
    //@InjectView(R.id.profile_dp)
    CircularImageView circular_profile_dp;
    ArrayAdapter<String> mAdapter;
    String mActivityTitle;
    Map<String, String> cookies;
    Map<String, String> formFields;
    ProgressDialog pd;
    SwipeRefreshLayout srl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendance_activity);
        ButterKnife.inject(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Attendance");

        //set_dp_photo();
        mActivityTitle = getTitle().toString();
        addDrawerItems();
        setupDrawer();

        cookies = new HashMap<>();
        formFields = new HashMap<>();

        srl = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pd = ProgressDialog.show(AttendanceActivity.this, "", "Loading Attendance details", true);
                SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                String reg_no = sp.getString("reg_no", "");
                String password = sp.getString("password", "");
                new AcademicsLogin().execute(reg_no, password);

            }
        });
        srl.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        // In case certificate are Expired.
        setTrustAllCerts();
        //display_marks(true);
        set_attend_adapter(true);
    }

    public void setTrustAllCerts() {
        /**
         * To Bypass SSL Certificate Check. In case the Academics Portal Certificate is expired.
         */
        try {
            // Create a custom Trust Manager
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };


            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                        public boolean verify(String urlHostName, SSLSession session) {
                            return true;
                        }
                    });
        } catch (Exception e) {
            //We can not recover from this exception.
            //e.printStackTrace();
        }
    }

    public class AcademicsLogin extends AsyncTask<String, String, String> {
        /**
         * Does the Academics Login requires Reg No and Password as arguments
         */

        Bitmap bm;
        Connection.Response resp;
        String cap_val = "";

        @Override
        protected void onPreExecute() {
            /**
             * On Pre is executed before Background. Count Down Timer is scheduled so that
             * If Login is not successful until 1 min. Async task is cancelled.
             */
            super.onPreExecute();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
            Log.i("Login", " PreExecute");

        }

        @Override
        protected String doInBackground(String... strings) {
            /**
             * Returns  "Success" -> Login successfully.
             *          "Failed" -> UpdatedLinksTable could not be updated successfully.
             *          "Wrong Password" -> When user's Reg No. and Passwords do not match.
             * Does Login for academics.
             * Stores cookies returned in cookies variable.
             */

            String reg_no = strings[0];
            String password = strings[1];

            try {
                Log.i("Login", " 0%");
                resp = Jsoup.connect(ACCESS_URL)
                        .timeout(100000)
                        .method(Connection.Method.GET)
                        .userAgent("Mozilla/5.0").execute();

                // Get request to login form of Academics Page, and store cookies, as well as their form fields.
                // Cookies are important. We will use them everywhere.

                Log.i("Login", "10%");
                cookies = resp.cookies();

                Elements fields = resp.parse().select("form input");
                formFields = new HashMap<>();
                for (Element field : fields) {
                    if (!field.attr("name").equals("")) {
                        formFields.put(field.attr("name"), field.attr("value"));
                    }
                }

                // Post request to Captcha page to retrieve Captcha Image, using cookies.
                Log.i("Login", "20%");

                Connection.Response resultImageResponse = Jsoup.connect(CAPTCHA_URL)
                        .cookies(cookies)
                        .ignoreContentType(true)
                        .method(Connection.Method.GET).timeout(100000).execute();

                Log.i("Login", "30%");
                byte[] bitmap_Data = resultImageResponse.bodyAsBytes();
                bm = BitmapFactory.decodeByteArray(bitmap_Data, 0, bitmap_Data.length);

                // Send this Bitmap to CaptchaParser.java to parse it,
                // It returns captcha value as XXXXXX.

                CaptchaParser cp = new CaptchaParser();
                cp.setBm(bm);
                cap_val = cp.getCaptcha();

                // Enter the details in form fields
                Log.i("Login", "40%");

                formFields.put("regno", reg_no);
                formFields.put("passwd", password);
                formFields.put("vrfcd", cap_val);

                // Get Reg No and Password and make login Login using Post request with cookies.
                Log.i("Login", "40%");

                Connection.Response access_url_resp = Jsoup.connect(SUBMIT_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                                // Extra headers
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Host", "academics.vit.ac.in")
                        .header("Connection", "keep-alive")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_login.asp")
                        .cookies(cookies)
                        .followRedirects(true)
                        .data(formFields)
                        .timeout(100000)
                        .method(Connection.Method.POST).execute();

                // Check if logged in by checking if cookies returned has reg number in it.
                // If Reg No was there, then Academics server accepted Login. Otherwise throw Exception.
                Log.i("Login", "45%");

                boolean logged_in = false;
                Map<String, String> temp = access_url_resp.cookies();
                for (String key : temp.keySet()) {
                    if (key.equals("logstudregno")) {
                        if (temp.get(key).equals(reg_no)) {
                            logged_in = true;
                        }
                    }
                    cookies.put(key, temp.get(key));
                }

                if (logged_in) {
                    SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.clear();
                    editor.putString("reg_no", reg_no);
                    editor.putString("password", password);
                    editor.apply();
                } else {
                    throw new WrongPassword();
                }
                Log.i("Login", "100%");


            } catch (WrongPassword wp) {
                // Password Wrong Caught here.
                //wp.printStackTrace();
                return "Wrong Password";
            } catch (Exception e) {
                // Any Connection Error Caught here.
                //e.printStackTrace();
                return "Network Error";
            }
            return "Success";
        }

        @Override
        protected void onPostExecute(String s) {
            /**
             * Next step, Retrieve all data from all courses.
             */

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            switch (s) {
                case "Success":
                    new GetAttendanceData().execute();
                    break;
                case "Network Error":
                    srl.setRefreshing(false);
                    pd.dismiss();
                    Toast.makeText(AttendanceActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
                    break;
                case "Wrong Password":
                    srl.setRefreshing(false);
                    pd.dismiss();
                    Toast.makeText(AttendanceActivity.this, "Please Check Credentials", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    public class GetAttendanceData extends AsyncTask<String, String, String> {

        Connection.Response resp;

        /**
         * Does the extraction of Spotlight data
         *
         * @return "Success" -> Profile data extracted.
         * "Failed" -> Unable to extract data.
         */
        @Override
        protected String doInBackground(String... param) {

            try {

                Log.i("GET Profile", "10%");

                resp = Jsoup.connect(ATTENDANCE_URL + "?sem=FS&fmdt=&todt=")
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Connection", "keep-alive")
                        .header("Host", "academics.vit.ac.in")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_menu.asp")
                        .cookies(cookies)
                        .ignoreContentType(false)
                        .timeout(100000).followRedirects(true)
                        .method(Connection.Method.GET).execute();

                if (resp.statusCode() == 200) {

                    resp = Jsoup.connect(ATTENDANCE_URL + "?sem=FS&fmdt=&todt=")
                            .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                            .header("Accept-Encoding", "gzip, deflate")
                            .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                            .header("Connection", "keep-alive")
                            .header("Host", "academics.vit.ac.in")
                            .header("DNT", "1")
                            .header("Referer", "https://academics.vit.ac.in/student/stud_menu.asp")
                            .cookies(cookies)
                            .ignoreContentType(false)
                            .timeout(100000).followRedirects(true)
                            .method(Connection.Method.GET).execute();
                    Log.i("GET Profile", "20%");
                } else {
                    return "Failed";
                }


                Document doc = resp.parse();
                Elements fields = doc.select("form input");
                formFields = new HashMap<>();
                for (Element field : fields) {
                    if (!field.attr("name").equals("")) {
                        formFields.put(field.attr("name"), field.attr("value"));
                        Log.i("input fields:", "added " + field.attr("name") + " " + field.attr("value"));
                    }
                }


                String frm_date = formFields.get("from_date");
                String to_date;

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
                to_date = df.format(cal.getTime());

                Log.i("Attendance", frm_date + to_date);

                resp = Jsoup.connect(ATTENDANCE_URL + "?sem=FS&fmdt=" + frm_date + "&todt=" + to_date)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Connection", "keep-alive")
                        .header("Host", "academics.vit.ac.in")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_menu.asp")
                        .cookies(cookies)
                        .ignoreContentType(false)
                        .timeout(100000).followRedirects(true)
                        .method(Connection.Method.GET).execute();

                Log.i("GET Profile", "40%");
                doc = resp.parse();
                Element att_table = doc.select("table[width=\"900\"]").first();
                Elements all_details = att_table.select("tr[bgcolor=\"#E6F2FF\"]");

                // Delete all if more than 1 row found
                if (all_details.size() > 0) {
                    AttendanceMiniTable.deleteAll(AttendanceMiniTable.class);
                } else {
                    return "Failed";
                }

                for (Element row : all_details) {
                    formFields = new HashMap<>();
                    Iterator<Element> col = row.select("td").iterator();

                    col.next().text(); // Sl No
                    col.next().text(); // Course Code
                    col.next().text(); // Course Title
                    col.next().text(); // Course Type
                    col.next().text(); // Course Slot
                    col.next().text(); // Course Reg Date
                    String attend_classes = col.next().text();
                    String total_classes = col.next().text();
                    String att_per = col.next().text();
                    String deb_status = col.next().text();
                    Element view_bt = col.next();
                    String classnbr = view_bt.select("input[name=\"classnbr\"]").first().val();
                    for (Element button : view_bt.select("input")) {
                        //data += button.attr("name") + " => " + button.val();
                        if (!button.attr("name").equals("")) {
                            formFields.put(button.attr("name"), button.val());
                            Log.i("attend", button.attr("name") + " => " + button.val());
                        }
                    }

                    AttendanceMiniTable att_mini_table = new AttendanceMiniTable(
                            Integer.parseInt(classnbr), Integer.parseInt(attend_classes),
                            Integer.parseInt(total_classes), att_per, deb_status);
                    att_mini_table.save();

                    Document detail_doc = Jsoup.connect(ATTENDANCE_DETAIL_URL)
                            .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                            .header("Accept-Encoding", "gzip, deflate")
                            .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                            .header("Connection", "keep-alive")
                            .header("Host", "academics.vit.ac.in")
                            .header("DNT", "1")
                            .header("Referer", "https://academics.vit.ac.in/student/stud_menu.asp")
                            .cookies(cookies)
                            .data(formFields)
                            .ignoreContentType(false)
                            .timeout(100000).followRedirects(true)
                            .method(Connection.Method.POST).execute().parse();

                    Elements detail_rows = detail_doc.select("table[cellpadding=\"4\"]").get(1).select("tr");

                    // Check if rows are there
                    if (detail_rows.size() > 0) {
                        // If more than 1 delete data
                        List<AttendanceDetailedTable> del_detailed = AttendanceDetailedTable.find(AttendanceDetailedTable.class,
                                "classnbr=?", classnbr);
                        for (AttendanceDetailedTable curr_ : del_detailed) {
                            curr_.delete();
                        }

                        // Store left out data.
                        for (Element detail_row : detail_rows) {
                            if (!detail_row.attr("bgcolor").equals("#5A768D")) {
                                Iterator<Element> det_col = detail_row.select("td").iterator();
                                String slno = det_col.next().text(); // Sl No
                                String date = det_col.next().text(); // Date
                                String slot = det_col.next().text(); // Slot
                                String class_status = det_col.next().text();
                                String class_unit = det_col.next().text();
                                String reason = det_col.next().text();
                                AttendanceDetailedTable add_detail = new AttendanceDetailedTable(
                                        slno, date, slot, class_status, class_unit, reason,
                                        Integer.parseInt(classnbr));
                                add_detail.save();
                            }
                        }
                    }
                    Log.i("detail", "completed");
                }

//                */
                return "Success";

            } catch (Exception e) {
                e.printStackTrace();
                return "Failed";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("Success")) {
                set_attend_adapter(false);
            }
            pd.dismiss();
            srl.setRefreshing(false);

        }
    }

    public class WrongPassword extends Exception {
        public WrongPassword() {
        }

        @Override
        public String toString() {
            return "Wrong Password Entered";
        }
    }

    private void set_attend_adapter(boolean repeat){

        // Get all course details from table
        Iterator<CoursesTable> courses = CoursesTable.findAll(CoursesTable.class);
        Iterator<AttendanceMiniTable> att_mini = AttendanceMiniTable.findAll(AttendanceMiniTable.class);

        if (!att_mini.hasNext() && repeat) {
            SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
            String reg_no = sp.getString("reg_no", "");
            String password = sp.getString("password", "");
            new AcademicsLogin().execute(reg_no, password);
            pd = ProgressDialog.show(AttendanceActivity.this, "", "Loading Attendance details", true);
            return;
        }

        String[] tmp_course_codes = new String[30];
        String[] tmp_course_nbr = new String[30];
        String[] tmp_course_names = new String[30];
        String[] tmp_att_summary = new String[30];
        int i = 0;
        while (courses.hasNext()) {
            CoursesTable temp = courses.next();
            AttendanceMiniTable att_tmp = att_mini.next();
            tmp_course_names[i] = temp.course_name;
            tmp_course_codes[i] = temp.course_code;
            tmp_course_nbr[i] = temp.class_nbr + "";
            tmp_att_summary[i] = att_tmp.attend_classes + "/" + att_tmp.total_classes + " : " + att_tmp.per_class;
            i += 1;
        }

        String[] course_names = new String[i];
        String[] course_codes = new String[i];
        String[] attend_summary = new String[i];
        String[] course_nbr = new String[i];

        for (int j = 0; j < i; j++) {
            course_names[j] = tmp_course_names[j];
            course_codes[j] = tmp_course_codes[j];
            attend_summary[j] = tmp_att_summary[j];
            course_nbr[j] = tmp_course_nbr[j];
        }


        // Sort the courses by their names.
        for (int j = 0; j < course_names.length; j++) {
            for (i = j + 1; i < course_names.length; i++) {
                if (course_names[i].compareToIgnoreCase(course_names[j]) < 0) {
                    String temp = course_names[j];
                    String temp_code = course_codes[j];
                    String temp_nbr = course_nbr[j];
                    String temp_a_name = attend_summary[j];
                    course_names[j] = course_names[i];
                    course_codes[j] = course_codes[i];
                    course_nbr[j] = course_nbr[i];
                    attend_summary[j] = attend_summary[i];
                    course_names[i] = temp;
                    course_codes[i] = temp_code;
                    attend_summary[i] = temp_a_name;
                    course_nbr[i] = temp_nbr;
                }
            }
        }

        // Add details to ListView Adapter
        ListView lv = (ListView) findViewById(R.id.courses_list);
        lv.setAdapter(new AttendanceListAdapter(this, course_names, course_codes, attend_summary, course_nbr));
    }

    private void set_dp_photo() {

        File root = android.os.Environment.getExternalStorageDirectory();
        File file = new File(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
        if (file.exists()) {
            circular_profile_dp.setSelectorStrokeWidth(10);
            circular_profile_dp.addShadow();
            Bitmap bm = BitmapFactory.decodeFile(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
            bm = Bitmap.createBitmap(bm, 0, 10, bm.getWidth(), bm.getWidth());
            circular_profile_dp.setImageBitmap(bm);
        }
    }

    private void addDrawerItems() {
        String[] osArray = { "TimeTable", "Marks", "SpotLight", "Course Page", "Faculty Login", "Attendance", "APT Attendance", "Academic Grades", "Adviser", "Profile", "Settings"};
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(AttendanceActivity.this, TimeTableActivity.class));
                        finish();
                        break;
                    case 1:
                        startActivity(new Intent(AttendanceActivity.this, MarksActivity.class));
                        finish();
                        break;
                    case 2:
                        startActivity(new Intent(AttendanceActivity.this, SpotlightActivity.class));
                        finish();
                        break;
                    case 3:
                        startActivity(new Intent(AttendanceActivity.this, AllCoursesListActivity.class));
                        finish();
                        break;
                    case 4:
                        startActivity(new Intent(AttendanceActivity.this, MainPageActivity.class));
                        finish();
                        break;
                    case 5:
                        //startActivity(new Intent(AttendanceActivity.this, AttendanceActivity.class));
                        //finish();
                        break;
                    case 6:
                        startActivity(new Intent(AttendanceActivity.this, APTAttendanceActivity.class));
                        finish();
                        break;
                    case 7:
                        startActivity(new Intent(AttendanceActivity.this, AcaGradesActivity.class));
                        finish();
                        break;
                    case 8:
                        startActivity(new Intent(AttendanceActivity.this, AdviserDetailsActivity.class));
                        finish();
                        break;
                    case 9:
                        startActivity(new Intent(AttendanceActivity.this, ProfileActivity.class));
                        finish();
                        break;
                    case 10:
                        startActivity(new Intent(AttendanceActivity.this, OptionsActivity.class));
                        finish();
                        break;
                }

            }
        });

    }

    private void setupDrawer() {

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("VITco");
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_options:

                startActivity(new Intent(AttendanceActivity.this, OptionsActivity.class));
                break;

            case R.id.action_feedback:

                // Calling doorbell
                new Doorbell(AttendanceActivity.this, 2159, "apBxZZVQyXmXsqAdTNhpLivrqwsjsjOljvvuu7wYGofddwEvBFEvULqJfshdN0zn").show();
                break;

            case R.id.action_logout:

                SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                SharedPreferences.Editor ed = sp.edit();
                ed.clear();
                ed.apply();

                AcaGradesTable.deleteAll(AcaGradesTable.class);
                APTAttendanceTable.deleteAll(APTAttendanceTable.class);
                AttendanceDetailedTable.deleteAll(AttendanceDetailedTable.class);
                AttendanceMiniTable.deleteAll(AttendanceMiniTable.class);
                CoursesTable.deleteAll(CoursesTable.class);
                ImagesTable.deleteAll(ImagesTable.class);
                LinksTable.deleteAll(LinksTable.class);
                MarksTable.deleteAll(MarksTable.class);
                MessagesTable.deleteAll(MessagesTable.class);
                OtherDetailsTable.deleteAll(OtherDetailsTable.class);
                SpotlightTable.deleteAll(SpotlightTable.class);
                TimeTableTable.deleteAll(TimeTableTable.class);
                UpdatedLinksTable.deleteAll(UpdatedLinksTable.class);

                startActivity(new Intent(AttendanceActivity.this, LoginActivity.class));
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }



}
