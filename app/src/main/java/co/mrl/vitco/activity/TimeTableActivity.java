package co.mrl.vitco.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.pkmmte.view.CircularImageView;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.mrl.vitco.R;
import co.mrl.vitco.adapter.PageAdapter;
import co.mrl.vitco.db_tables.APTAttendanceTable;
import co.mrl.vitco.db_tables.AcaGradesTable;
import co.mrl.vitco.db_tables.AttendanceDetailedTable;
import co.mrl.vitco.db_tables.AttendanceMiniTable;
import co.mrl.vitco.db_tables.CoursesTable;
import co.mrl.vitco.db_tables.ImagesTable;
import co.mrl.vitco.db_tables.LinksTable;
import co.mrl.vitco.db_tables.MarksTable;
import co.mrl.vitco.db_tables.MessagesTable;
import co.mrl.vitco.db_tables.OtherDetailsTable;
import co.mrl.vitco.db_tables.SpotlightTable;
import co.mrl.vitco.db_tables.TimeTableTable;
import co.mrl.vitco.db_tables.UpdatedLinksTable;
import co.mrl.vitco.model.CaptchaParser;
import co.mrl.vitco.tabs.TabFragment;
import io.doorbell.android.Doorbell;

/**
 * Created by root on 20/10/15.
 */
public class TimeTableActivity extends AppCompatActivity {

    public static final String TIMETABLE_URL = "https://academics.vit.ac.in/student/timetable_ws.asp";
    public static final String ACCESS_URL = "https://academics.vit.ac.in/student/stud_login.asp";
    public static final String SUBMIT_URL = "https://academics.vit.ac.in/student/stud_login_submit.asp";
    public static final String CAPTCHA_URL = "https://academics.vit.ac.in/student/captcha.asp";
    public static final String PATH_NAME = "/VITco/Images/";
    @InjectView(R.id.nav_list)
    ListView mDrawerList;
    ActionBarDrawerToggle mDrawerToggle;
    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @InjectView(R.id.pager)
    ViewPager viewPager;
    //@Optional
    //@InjectView(R.id.profile_dp)
    CircularImageView circular_profile_dp;
    ArrayAdapter<String> mAdapter;
    String mActivityTitle;
    Map<String, String> cookies;
    Map<String, String> formFields;
    ProgressDialog pd;
    SwipeRefreshLayout srl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timetable_activity);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        ButterKnife.inject(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("TimeTable");

        //set_dp_photo();
        mActivityTitle = getTitle().toString();
        addDrawerItems();
        setupDrawer();
        // In case certificate are Expired.
        setTrustAllCerts();
        //display_timetable_data(true);
        display_data(true);


        cookies = new HashMap<>();
        formFields = new HashMap<>();

        srl = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pd = ProgressDialog.show(TimeTableActivity.this, "", "Loading Timetable Details", true);
                SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                String reg_no = sp.getString("reg_no", "");
                String password = sp.getString("password", "");
                new AcademicsLogin().execute(reg_no, password);
            }
        });
        srl.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void display_data(boolean repeat) {

        // Check for both Monday and Tuesday, if no classes are there refresh it.
        List<TimeTableTable> retrieve_data = TimeTableTable.find(TimeTableTable.class,
                "day=?", "MON");
        if (retrieve_data.size() <= 0 && repeat) {
            Log.i("timetable retirieve", "You have data" + retrieve_data.size());
            retrieve_data = TimeTableTable.find(TimeTableTable.class,
                    "day=?", "TUE");
            if (retrieve_data.size() <= 0) {

                pd = ProgressDialog.show(TimeTableActivity.this, "", "Loading Timetable Details", true);
                SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                String reg_no = sp.getString("reg_no", "");
                String password = sp.getString("password", "");
                new AcademicsLogin().execute(reg_no, password);
                return;
            }
        }

        Log.i("timetable retirieve", "You have data" + retrieve_data.size());
        // Else if classes found show them
        PageAdapter adapter = new PageAdapter(getSupportFragmentManager());
        // Monday tab
        TabFragment tb = new TabFragment();
        Bundle send_bundle = new Bundle();
        send_bundle.putString("day_name", "MON");
        tb.setArguments(send_bundle);
        adapter.add_frag(tb, "MONDAY");
        // Tuesday tab
        tb = new TabFragment();
        send_bundle = new Bundle();
        send_bundle.putString("day_name", "TUE");
        tb.setArguments(send_bundle);
        adapter.add_frag(tb, "TUESDAY");
        // Wednesday tab
        tb = new TabFragment();
        send_bundle = new Bundle();
        send_bundle.putString("day_name", "WED");
        tb.setArguments(send_bundle);
        adapter.add_frag(tb, "WEDNESDAY");
        // Thursday tab
        tb = new TabFragment();
        send_bundle = new Bundle();
        send_bundle.putString("day_name", "THU");
        tb.setArguments(send_bundle);
        adapter.add_frag(tb, "THURSDAY");
        // Friday tab
        tb = new TabFragment();
        send_bundle = new Bundle();
        send_bundle.putString("day_name", "FRI");
        tb.setArguments(send_bundle);
        adapter.add_frag(tb, "FRIDAY");
        // Saturday tab
        tb = new TabFragment();
        send_bundle = new Bundle();
        send_bundle.putString("day_name", "SAT");
        tb.setArguments(send_bundle);
        adapter.add_frag(tb, "SATURDAY");
        // set the adapter
        viewPager.setAdapter(adapter);
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_WEEK);
        if (day <= 2) {
            day = 0;
        } else {
            day = day - 2;
        }
        viewPager.setCurrentItem(day);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

    }

    public void setTrustAllCerts() {
        /**
         * To Bypass SSL Certificate Check. In case the Academics Portal Certificate is expired.
         */
        try {
            // Create a custom Trust Manager
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };


            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                        public boolean verify(String urlHostName, SSLSession session) {
                            return true;
                        }
                    });
        } catch (Exception e) {
            //We can not recover from this exception.
            //e.printStackTrace();
        }
    }

    public class AcademicsLogin extends AsyncTask<String, String, String> {
        /**
         * Does the Academics Login requires Reg No and Password as arguments
         */

        Bitmap bm;
        Connection.Response resp;
        String cap_val = "";

        @Override
        protected void onPreExecute() {
            /**
             * On Pre is executed before Background. Count Down Timer is scheduled so that
             * If Login is not successful until 1 min. Async task is cancelled.
             */
            super.onPreExecute();
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
            Log.i("Login", " PreExecute");

        }

        @Override
        protected String doInBackground(String... strings) {
            /**
             * Returns  "Success" -> Login successfully.
             *          "Failed" -> UpdatedLinksTable could not be updated successfully.
             *          "Wrong Password" -> When user's Reg No. and Passwords do not match.
             * Does Login for academics.
             * Stores cookies returned in cookies variable.
             */

            String reg_no = strings[0];
            String password = strings[1];

            try {
                Log.i("Login", " 0%");
                resp = Jsoup.connect(ACCESS_URL)
                        .timeout(100000)
                        .method(Connection.Method.GET)
                        .userAgent("Mozilla/5.0").execute();

                // Get request to login form of Academics Page, and store cookies, as well as their form fields.
                // Cookies are important. We will use them everywhere.

                Log.i("Login", "10%");
                cookies = resp.cookies();

                Elements fields = resp.parse().select("form input");
                formFields = new HashMap<>();
                for (Element field : fields) {
                    if (!field.attr("name").equals("")) {
                        formFields.put(field.attr("name"), field.attr("value"));
                    }
                }

                // Post request to Captcha page to retrieve Captcha Image, using cookies.
                Log.i("Login", "20%");

                Connection.Response resultImageResponse = Jsoup.connect(CAPTCHA_URL)
                        .cookies(cookies)
                        .ignoreContentType(true)
                        .method(Connection.Method.GET).timeout(100000).execute();

                Log.i("Login", "30%");
                byte[] bitmap_Data = resultImageResponse.bodyAsBytes();
                bm = BitmapFactory.decodeByteArray(bitmap_Data, 0, bitmap_Data.length);

                // Send this Bitmap to CaptchaParser.java to parse it,
                // It returns captcha value as XXXXXX.

                CaptchaParser cp = new CaptchaParser();
                cp.setBm(bm);
                cap_val = cp.getCaptcha();

                // Enter the details in form fields
                Log.i("Login", "40%");

                formFields.put("regno", reg_no);
                formFields.put("passwd", password);
                formFields.put("vrfcd", cap_val);

                // Get Reg No and Password and make login Login using Post request with cookies.
                Log.i("Login", "40%");

                Connection.Response access_url_resp = Jsoup.connect(SUBMIT_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                                // Extra headers
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Host", "academics.vit.ac.in")
                        .header("Connection", "keep-alive")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_login.asp")
                        .cookies(cookies)
                        .followRedirects(true)
                        .data(formFields)
                        .timeout(100000)
                        .method(Connection.Method.POST).execute();

                // Check if logged in by checking if cookies returned has reg number in it.
                // If Reg No was there, then Academics server accepted Login. Otherwise throw Exception.
                Log.i("Login", "45%");

                boolean logged_in = false;
                Map<String, String> temp = access_url_resp.cookies();
                for (String key : temp.keySet()) {
                    if (key.equals("logstudregno")) {
                        if (temp.get(key).equals(reg_no)) {
                            logged_in = true;
                        }
                    }
                    cookies.put(key, temp.get(key));
                }

                if (logged_in) {
                    SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.clear();
                    editor.putString("reg_no", reg_no);
                    editor.putString("password", password);
                    editor.apply();
                } else {
                    throw new WrongPassword();
                }
                Log.i("Login", "100%");


            } catch (WrongPassword wp) {
                // Password Wrong Caught here.
                //wp.printStackTrace();
                return "Wrong Password";
            } catch (Exception e) {
                // Any Connection Error Caught here.
                //e.printStackTrace();
                return "Network Error";
            }
            return "Success";
        }

        @Override
        protected void onPostExecute(String s) {
            /**
             * Next step, Retrieve all data from all courses.
             */

            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            switch (s) {
                case "Success":
                    new GetTimeTableData().execute();
                    break;
                case "Network Error":
                    srl.setRefreshing(false);
                    pd.dismiss();
                    Toast.makeText(TimeTableActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
                    break;
                case "Wrong Password":
                    srl.setRefreshing(false);
                    pd.dismiss();
                    Toast.makeText(TimeTableActivity.this, "Please Check Credentials", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    public class GetTimeTableData extends AsyncTask<String, String, String> {

        Connection.Response resp;

        /**
         * Does the extraction of Spotlight data
         *
         * @return "Success" -> Profile data extracted.
         * "Failed" -> Unable to extract data.
         */
        @Override
        protected String doInBackground(String... param) {

            try {

                Log.i("GET Profile", "10%");

                resp = Jsoup.connect(TIMETABLE_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Connection", "keep-alive")
                        .header("Host", "academics.vit.ac.in")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_menu.asp")
                        .cookies(cookies)
                        .ignoreContentType(false)
                        .timeout(1000 * 50).followRedirects(true)
                        .method(Connection.Method.GET).execute();

                if (resp.statusCode() == 200) {

                    resp = Jsoup.connect(TIMETABLE_URL)
                            .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                            .header("Accept-Encoding", "gzip, deflate")
                            .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                            .header("Connection", "keep-alive")
                            .header("Host", "academics.vit.ac.in")
                            .header("DNT", "1")
                            .header("Referer", "https://academics.vit.ac.in/student/stud_menu.asp")
                            .cookies(cookies)
                            .ignoreContentType(false)
                            .timeout(1000 * 50).followRedirects(true)
                            .method(Connection.Method.GET).execute();
                    Log.i("GET Profile", "20%");

                } else {
                    return "Failed";
                }

                Log.i("GET Profile", "40%");
                // Delete all timetable data
                TimeTableTable.deleteAll(TimeTableTable.class);

                Document doc = resp.parse();
                Elements tables = doc.select("table[cellpadding=\"2\"]");
                int pos_of_timetable = tables.size() - 1;
                Iterator<Element> timetable_rows = tables
                        .get(pos_of_timetable).select("tr[height=\"25\"]").iterator();
                timetable_rows.next();
                while (timetable_rows.hasNext()) {
                    String day = "";
                    for (Element col : timetable_rows.next().select("td")) {
                        if (col.attr("bgcolor").equals("#CCFF33")) {
                            // Found registered slot
                            String[] row_arr = col.text().split(" - ");
                            String course_code = row_arr[0];
                            String course_type = row_arr[1];
                            String room_no = row_arr[2];
                            String slot = row_arr[3];
                            TimeTableTable add_new = new TimeTableTable(day, slot, room_no, course_type, course_code);
                            add_new.save();
                        } else if (col.attr("bgcolor").equals("#C0C0C0")) {
                            // Day name column
                            day = col.text();
                        } else if (col.attr("bgcolor").equals("#FFFFFF") && col.attr("colspan").equals("3")) {
                            // Seminar / Meeting
                            TimeTableTable add_new = new TimeTableTable(day, " - ", " - ", " - ", "SEM000");
                            add_new.save();
                            add_new = new TimeTableTable(day, " - ", " - ", " - ", "SEM000");
                            add_new.save();
                            add_new = new TimeTableTable(day, " - ", " - ", " - ", "SEM000");
                            add_new.save();

                        } else {
                            // Nothing registered here
                            String[] row_arr = col.text().split("/");
                            String slot = row_arr[0];
                            TimeTableTable add_new = new TimeTableTable(day, slot, "==", "==", "==");
                            add_new.save();
                        }
                    }
                }

                // Get timetable, extract all courses details

                Elements all_rows = tables.get(0).select("tr[bgcolor=\"#EDEADE\"]");
                String sample, sample_code, sample_name, faculty_name;
                for (Element line : all_rows) {
                    Iterator<Element> tds = line.select("font[color=\"green\"]").iterator();
                    boolean test = false;
                    int class_nbr = -1605;
                    sample = "";
                    faculty_name = "";
                    sample_name = "";
                    sample_code = "";

                    try {
                        sample = tds.next().text();
                        class_nbr = Integer.parseInt(sample);
                        if (class_nbr > 999) {
                            sample_code = tds.next().text();
                            sample_name = tds.next().text();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            faculty_name = tds.next().text();
                            test = true;
                        }
                    } catch (Exception e) {
                        test = false;
                    }
                    if (!test) {
                        try {
                            sample = tds.next().text();
                            sample_code = tds.next().text();
                            sample_name = tds.next().text();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            faculty_name = tds.next().text();
                            class_nbr = Integer.parseInt(sample);
                        } catch (Exception e) {
                            //e.printStackTrace();
                        }
                    }
                    if (class_nbr != -1605) {

                        CoursesTable ct = new CoursesTable("cpc", class_nbr, sample_code, faculty_name, sample_name);
                        ct.save();
                    }
                }
                return "Success";

            } catch (Exception e) {
                e.printStackTrace();
                return "Failed";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("Success")) {
                display_data(false);
            }
            pd.dismiss();
            srl.setRefreshing(false);
        }
    }


    public class WrongPassword extends Exception {
        public WrongPassword() {
        }

        @Override
        public String toString() {
            return "Wrong Password Entered";
        }
    }

    private void set_dp_photo() {

        File root = android.os.Environment.getExternalStorageDirectory();
        File file = new File(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
        if (file.exists()) {
            circular_profile_dp.setSelectorStrokeWidth(10);
            circular_profile_dp.addShadow();
            Bitmap bm = BitmapFactory.decodeFile(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
            bm = Bitmap.createBitmap(bm, 0, 10, bm.getWidth(), bm.getWidth());
            circular_profile_dp.setImageBitmap(bm);
        }
    }

    private void addDrawerItems() {
        String[] osArray = {"TimeTable", "Marks", "SpotLight", "Course Page", "Faculty Login", "Attendance", "APT Attendance", "Academic Grades", "Adviser", "Profile", "Settings"};
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        //startActivity(new Intent(TimeTableActivity.this, TimeTableActivity.class));
                        //finish();
                        break;
                    case 1:
                        startActivity(new Intent(TimeTableActivity.this, MarksActivity.class));
                        finish();
                        break;
                    case 2:
                        startActivity(new Intent(TimeTableActivity.this, SpotlightActivity.class));
                        finish();
                        break;
                    case 3:
                        startActivity(new Intent(TimeTableActivity.this, AllCoursesListActivity.class));
                        finish();
                        break;
                    case 4:
                        startActivity(new Intent(TimeTableActivity.this, MainPageActivity.class));
                        finish();
                        break;
                    case 5:
                        startActivity(new Intent(TimeTableActivity.this, AttendanceActivity.class));
                        finish();
                        break;
                    case 6:
                        startActivity(new Intent(TimeTableActivity.this, APTAttendanceActivity.class));
                        finish();
                        break;
                    case 7:
                        startActivity(new Intent(TimeTableActivity.this, AcaGradesActivity.class));
                        finish();
                        break;
                    case 8:
                        startActivity(new Intent(TimeTableActivity.this, AdviserDetailsActivity.class));
                        finish();
                        break;
                    case 9:
                        startActivity(new Intent(TimeTableActivity.this, ProfileActivity.class));
                        finish();
                        break;
                    case 10:
                        startActivity(new Intent(TimeTableActivity.this, OptionsActivity.class));
                        finish();
                        break;
                }

            }
        });

    }

    private void setupDrawer() {

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("VITco");
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_options:

                startActivity(new Intent(TimeTableActivity.this, OptionsActivity.class));
                break;

            case R.id.action_feedback:

                // Calling doorbell
                new Doorbell(TimeTableActivity.this, 2159, "apBxZZVQyXmXsqAdTNhpLivrqwsjsjOljvvuu7wYGofddwEvBFEvULqJfshdN0zn").show();
                break;

            case R.id.action_logout:

                SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                SharedPreferences.Editor ed = sp.edit();
                ed.clear();
                ed.apply();

                AcaGradesTable.deleteAll(AcaGradesTable.class);
                APTAttendanceTable.deleteAll(APTAttendanceTable.class);
                AttendanceDetailedTable.deleteAll(AttendanceDetailedTable.class);
                AttendanceMiniTable.deleteAll(AttendanceMiniTable.class);
                CoursesTable.deleteAll(CoursesTable.class);
                ImagesTable.deleteAll(ImagesTable.class);
                LinksTable.deleteAll(LinksTable.class);
                MarksTable.deleteAll(MarksTable.class);
                MessagesTable.deleteAll(MessagesTable.class);
                OtherDetailsTable.deleteAll(OtherDetailsTable.class);
                SpotlightTable.deleteAll(SpotlightTable.class);
                TimeTableTable.deleteAll(TimeTableTable.class);
                UpdatedLinksTable.deleteAll(UpdatedLinksTable.class);

                startActivity(new Intent(TimeTableActivity.this, LoginActivity.class));
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }


}
