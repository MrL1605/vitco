package co.mrl.vitco.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.pkmmte.view.CircularImageView;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.mrl.vitco.R;
import co.mrl.vitco.adapter.CourseListAdapter;
import co.mrl.vitco.db_tables.APTAttendanceTable;
import co.mrl.vitco.db_tables.AcaGradesTable;
import co.mrl.vitco.db_tables.AttendanceDetailedTable;
import co.mrl.vitco.db_tables.AttendanceMiniTable;
import co.mrl.vitco.db_tables.CoursesTable;
import co.mrl.vitco.db_tables.ImagesTable;
import co.mrl.vitco.db_tables.LinksTable;
import co.mrl.vitco.db_tables.MarksTable;
import co.mrl.vitco.db_tables.MessagesTable;
import co.mrl.vitco.db_tables.OtherDetailsTable;
import co.mrl.vitco.db_tables.SpotlightTable;
import co.mrl.vitco.db_tables.TimeTableTable;
import co.mrl.vitco.db_tables.UpdatedLinksTable;
import co.mrl.vitco.model.CaptchaParser;
import io.doorbell.android.Doorbell;

/**
 * Created by lalit on 5/9/15.
 */

@SuppressWarnings("deprecation")
public class AllCoursesListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static final String ACCESS_URL = "https://academics.vit.ac.in/student/stud_login.asp";
    public static final String SUBMIT_URL = "https://academics.vit.ac.in/student/stud_login_submit.asp";
    public static final String CAPTCHA_URL = "https://academics.vit.ac.in/student/captcha.asp";
    public static final String TIMETABLE_URL = "https://academics.vit.ac.in/student/timetable_ws.asp";
    public static final String ITER_COURSE_PAGE_URL = "https://academics.vit.ac.in/student/coursepage_view.asp?sem=WS&slt=&fac=&crs=";
    public static final String PATH_NAME = "/VITco/Images/";
    @InjectView(R.id.nav_list)
    ListView mDrawerList;
    ActionBarDrawerToggle mDrawerToggle;
    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    //@Optional
    //@InjectView(R.id.profile_dp)
    CircularImageView circular_profile_dp;
    ArrayAdapter<String> mAdapter;
    String mActivityTitle;
    Map<String, String> cookies;
    Map<String, String> formFields;
    Map<String, String> code_nbr;
    Map<String, String> code_name;
    Map<String, String> code_f_name;
    ListView lv;
    SwipeRefreshLayout srl;
    String[] course_names;
    String[] course_codes;
    String[] course_nbr;
    String[] faculty_names;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /**
         * Main Screen after Logging in to VITco. OnCreate is called at first.
         * Variables are Initialized. And List adapter is given array of details for Courses.
         */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_courses_list_activity);
        ButterKnife.inject(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Course Page");

        //set_dp_photo();
        mActivityTitle = getTitle().toString();
        addDrawerItems();
        setupDrawer();

        // In case certificate are Expired.
        setTrustAllCerts();

        // Initialize variables
        cookies = new HashMap<>();
        formFields = new HashMap<>();
        code_nbr = new HashMap<>();
        code_name = new HashMap<>();
        code_f_name = new HashMap<>();

        srl = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        srl.setOnRefreshListener(this);
        srl.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        // Get all course details from table
        Iterator<CoursesTable> courses = CoursesTable.findAll(CoursesTable.class);
        String[] tmp_course_codes = new String[30];
        String[] tmp_course_nbr = new String[30];
        String[] tmp_course_names = new String[30];
        String[] tmp_faculty_names = new String[30];
        int i = 0;
        while (courses.hasNext()) {
            CoursesTable temp = courses.next();
            tmp_course_names[i] = temp.course_name;
            tmp_course_codes[i] = temp.course_code;
            tmp_course_nbr[i] = temp.class_nbr + "";
            tmp_faculty_names[i] = temp.faculty_name;
            i += 1;
        }
        course_names = new String[i];
        course_codes = new String[i];
        faculty_names = new String[i];
        course_nbr = new String[i];

        for (int j = 0; j < i; j++) {
            course_names[j] = tmp_course_names[j];
            course_codes[j] = tmp_course_codes[j];
            faculty_names[j] = tmp_faculty_names[j];
            course_nbr[j] = tmp_course_nbr[j];
        }

        // Sort the courses by their names.
        for (int j = 0; j < course_names.length; j++) {
            for (i = j + 1; i < course_names.length; i++) {
                if (course_names[i].compareToIgnoreCase(course_names[j]) < 0) {
                    String temp = course_names[j];
                    String temp_code = course_codes[j];
                    String temp_nbr = course_nbr[j];
                    String temp_f_name = faculty_names[j];
                    course_names[j] = course_names[i];
                    course_codes[j] = course_codes[i];
                    course_nbr[j] = course_nbr[i];
                    faculty_names[j] = faculty_names[i];
                    course_names[i] = temp;
                    course_codes[i] = temp_code;
                    faculty_names[i] = temp_f_name;
                    course_nbr[i] = temp_nbr;
                }
            }
        }

        // Add details to ListView Adapter
        lv = (ListView) findViewById(R.id.courses_list);
        lv.setAdapter(new CourseListAdapter(this, course_names, course_codes, faculty_names, course_nbr));
    }

    @Override
    public void onRefresh() {
        /**
         * On Refresh called when Screen is Refresh by swiping down the screen.
         * Get the Login details.
         * Do Login
         * Get all course details
         */
        SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
        String reg_no = sp.getString("reg_no", "");
        String password = sp.getString("password", "");
        new AcademicsLogin().execute(reg_no, password);
    }

    public void setTrustAllCerts() {
        /**
         * To Bypass SSL Certificate Check. In case the Academics Portal Certificate is expired.
         */
        try {
            // Create a custom Trust Manager
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };


            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                        public boolean verify(String urlHostName, SSLSession session) {
                            return true;
                        }
                    });
        } catch (Exception e) {
            //We can not recover from this exception.
            //e.printStackTrace();
        }
    }

    public class AcademicsLogin extends AsyncTask<String, String, String> {
        /**
         * Does the Academics Login requires Reg No and Password as arguments
         */

        Bitmap bm;
        Connection.Response resp;
        String cap_val = "";

        @Override
        protected void onPreExecute() {
            /**
             * On Pre is executed before Background. Count Down Timer is scheduled so that
             * If Login is not successful until 1 min. Async task is cancelled.
             */
            super.onPreExecute();
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

        }

        @Override
        protected String doInBackground(String... strings) {
            /**
             * Returns  "Success" -> Login successfully.
             *          "Failed" -> UpdatedLinksTable could not be updated successfully.
             *          "Wrong Password" -> When user's Reg No. and Passwords do not match.
             * Does Login for academics.
             * Stores cookies returned in cookies variable.
             */

            String reg_no = strings[0];
            String password = strings[1];

            try {
                resp = Jsoup.connect(ACCESS_URL)
                        .timeout(300000)
                        .method(Connection.Method.GET)
                        .userAgent("Mozilla/5.0").execute();

                // Get request to login form of Academics Page, and store cookies, as well as their form fields.
                // Cookies are important. We will use them everywhere.

                cookies = resp.cookies();

                Elements fields = resp.parse().select("form input");
                formFields = new HashMap<>();
                for (Element field : fields) {
                    if (!field.attr("name").equals("")) {
                        formFields.put(field.attr("name"), field.attr("value"));
                    }
                }

                // Post request to Captcha page to retrieve Captcha Image, using cookies.

                Connection.Response resultImageResponse = Jsoup.connect(CAPTCHA_URL)
                        .cookies(cookies)
                        .ignoreContentType(true)
                        .method(Connection.Method.GET).timeout(30000).execute();

                byte[] bitmap_Data = resultImageResponse.bodyAsBytes();
                bm = BitmapFactory.decodeByteArray(bitmap_Data, 0, bitmap_Data.length);

                // Send this Bitmap to CaptchaParser.java to parse it,
                // It returns captcha value as XXXXXX.

                CaptchaParser cp = new CaptchaParser();
                cp.setBm(bm);
                cap_val = cp.getCaptcha();

                // Enter the details in form fields

                formFields.put("regno", reg_no);
                formFields.put("passwd", password);
                formFields.put("vrfcd", cap_val);

                // Get Reg No and Password and make login Login using Post request with cookies.

                Connection.Response access_url_resp = Jsoup.connect(SUBMIT_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                                // Extra headers
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Host", "academics.vit.ac.in")
                        .header("Connection", "keep-alive")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_login.asp")
                        .cookies(cookies)
                        .followRedirects(true)
                        .data(formFields)
                        .timeout(300000)
                        .method(Connection.Method.POST).execute();

                // Check if logged in by checking if cookies returned has reg number in it.
                // If Reg No was there, then Academics server accepted Login. Otherwise throw Exception.

                boolean logged_in = false;
                Map<String, String> temp = access_url_resp.cookies();
                for (String key : temp.keySet()) {
                    if (key.equals("logstudregno")) {
                        if (temp.get(key).equals(reg_no)) {
                            logged_in = true;
                        }
                    }
                    cookies.put(key, temp.get(key));
                }

                if (logged_in) {
                    SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.clear();
                    editor.putString("reg_no", reg_no);
                    editor.putString("password", password);
                    editor.apply();
                } else {
                    throw new WrongPassword();
                }


            } catch (WrongPassword wp) {
                // Password Wrong Caught here.
                //wp.printStackTrace();
                return "Wrong Password";
            } catch (Exception e) {
                // Any Connection Error Caught here.
                //e.printStackTrace();
                return "Network Error";
            }
            return "Success";
        }

        @Override
        protected void onPostExecute(String s) {
            /**
             * Next step, Retrieve all data from all courses.
             */

            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            switch (s) {
                case "Success":
                    new ClassNbrListExtractor().execute();
                    break;
                case "Network Error":
                    srl.setRefreshing(false);
                    Toast.makeText(AllCoursesListActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
                    break;
                case "Wrong Password":
                    srl.setRefreshing(false);
                    Toast.makeText(AllCoursesListActivity.this, "Please Check Credentials", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    public class ClassNbrListExtractor extends AsyncTask<String, String, String> {
        /**
         * Extracts Class No for each course using the cookies from Login class.
         */

        @Override
        protected void onPreExecute() {
            /**
             * On Pre is executed before Background. Count Down Timer is scheduled so that
             * If Login is not successful until 1 min. Async task is cancelled.
             */

            super.onPreExecute();
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

        }

        @Override
        protected String doInBackground(String... strings) {
            /**
             * Returns  "Success" -> CoursesTable updated with new links successfully.
             *          "Failed" -> CoursesLinksTable could not be updated successfully.
             * Retrieves all courses available in Timetable with class nbr to be stored in DB.
             */

            try {
                Connection.Response timetable_rep = Jsoup.connect(TIMETABLE_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Connection", "keep-alive")
                        .header("Host", "academics.vit.ac.in")
                        .header("DNT", "1")
                        .cookies(cookies)
                        .ignoreContentType(false)
                        .timeout(300000).followRedirects(true)
                        .method(Connection.Method.GET).execute();

                // If first try is Object moved
                // then do second try
                if (timetable_rep.statusCode() == 200) {
                    timetable_rep = Jsoup.connect(TIMETABLE_URL)
                            .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                            .header("Accept-Encoding", "gzip, deflate")
                            .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                            .header("Connection", "keep-alive")
                            .header("Host", "academics.vit.ac.in")
                            .header("DNT", "1")
                            .cookies(cookies)
                            .ignoreContentType(false)
                            .timeout(300000).followRedirects(true)
                            .method(Connection.Method.GET).execute();
                }

                // Get timetable, extract all courses details

                Document document = timetable_rep.parse();
                Elements all_rows = document.select("tr[bgcolor=\"#EDEADE\"]");
                String sample, sample_code, sample_name, faculty_name;
                for (Element line : all_rows) {
                    Iterator<Element> tds = line.select("font[color=\"green\"]").iterator();
                    boolean test = false;
                    int class_nbr = -1605;
                    sample = "";
                    faculty_name = "";
                    sample_name = "";
                    sample_code = "";

                    try {
                        sample = tds.next().text();
                        class_nbr = Integer.parseInt(sample);
                        if (class_nbr > 999) {
                            sample_code = tds.next().text();
                            sample_name = tds.next().text();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            faculty_name = tds.next().text();
                            test = true;
                        }
                    } catch (Exception e) {
                        test = false;
                    }
                    if (!test) {
                        try {
                            sample = tds.next().text();
                            sample_code = tds.next().text();
                            sample_name = tds.next().text();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            faculty_name = tds.next().text();
                            class_nbr = Integer.parseInt(sample);
                        } catch (Exception e) {
                            //e.printStackTrace();
                        }
                    }
                    if (class_nbr != -1605) {
                        code_nbr.put("" + class_nbr, sample_code);
                        code_f_name.put("" + class_nbr, faculty_name);
                        code_name.put("" + class_nbr, sample_name);
                    }
                }

            } catch (Exception e) {
                //e.printStackTrace();
                return "Failed";
            }
            return "Success";
        }

        @Override
        protected void onPostExecute(String s) {

            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            if (s.equals("Success")) {
                new ClassPlanCodeExtraction().execute();
            } else if (s.equals("Failed")) {
                srl.setRefreshing(false);
                Toast.makeText(AllCoursesListActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
            }

        }
    }

    public class ClassPlanCodeExtraction extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

        }

        @Override
        protected String doInBackground(String... strings) {

            String db_course_code, db_class_nbr, db_f_name, db_course_name;
            CoursesTable.deleteAll(CoursesTable.class);

            try {
                for (String nbr_key : code_nbr.keySet()) {
                    db_class_nbr = nbr_key;
                    db_course_code = code_nbr.get(nbr_key);
                    db_f_name = code_f_name.get(nbr_key);
                    db_course_name = code_name.get(nbr_key);

                    String course_url = ITER_COURSE_PAGE_URL + db_course_code;

                    Connection.Response marks_resp = Jsoup.connect(course_url)
                            .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                            .header("Accept-Encoding", "gzip, deflate")
                            .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                            .header("Connection", "keep-alive")
                            .header("Host", "academics.vit.ac.in")
                            .header("DNT", "1")
                            .cookies(cookies)
                            .ignoreContentType(false)
                            .timeout(300000).followRedirects(true)
                            .method(Connection.Method.GET).execute();

                    Document doc = marks_resp.parse();
                    String course_plan_code, class_nbr;
                    Elements all_rows = doc.select("tr[bgcolor=\"#E1ECF2\"]");

                    for (Element row : all_rows) {
                        class_nbr = row.select("td[width=\"100\"],[align=\"center\"]").eq(1).text();
                        course_plan_code = row.select("input [type=\"hidden\"],[name=\"crsplancode\"]").first().val();
                        if (db_class_nbr.equals(class_nbr)) {
                            CoursesTable new_add = new CoursesTable(course_plan_code, Integer.parseInt(class_nbr), db_course_code, db_f_name, db_course_name);
                            new_add.save();
                        }
                    }
                }


            } catch (Exception e) {
                //e.printStackTrace();
                return "Failed";
            }
            return "Success";
        }

        @Override
        protected void onPostExecute(String s) {

            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            srl.setRefreshing(false);
            if (s.equals("Failed")) {
                Toast.makeText(AllCoursesListActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
            }

        }
    }

    public class WrongPassword extends Exception {
        public WrongPassword() {
        }

        @Override
        public String toString() {
            return "Wrong Password Entered";
        }
    }


    private void set_dp_photo() {

        File root = android.os.Environment.getExternalStorageDirectory();
        File file = new File(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
        if (file.exists()) {
            circular_profile_dp.setSelectorStrokeWidth(10);
            circular_profile_dp.addShadow();
            Bitmap bm = BitmapFactory.decodeFile(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
            bm = Bitmap.createBitmap(bm, 0, 10, bm.getWidth(), bm.getWidth());
            circular_profile_dp.setImageBitmap(bm);
        }
    }

    private void addDrawerItems() {
        String[] osArray = { "TimeTable", "Marks", "SpotLight", "Course Page", "Faculty Login", "Attendance", "APT Attendance", "Academic Grades", "Adviser", "Profile", "Settings"};
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(AllCoursesListActivity.this, TimeTableActivity.class));
                        finish();
                        break;
                    case 1:
                        startActivity(new Intent(AllCoursesListActivity.this, MarksActivity.class));
                        finish();
                        break;
                    case 2:
                        startActivity(new Intent(AllCoursesListActivity.this, SpotlightActivity.class));
                        finish();
                        break;
                    case 3:
                        //startActivity(new Intent(AllCoursesListActivity.this, AllCoursesListActivity.class));
                        //finish();
                        break;
                    case 4:
                        startActivity(new Intent(AllCoursesListActivity.this, MainPageActivity.class));
                        finish();
                        break;
                    case 5:
                        startActivity(new Intent(AllCoursesListActivity.this, AttendanceActivity.class));
                        finish();
                        break;
                    case 6:
                        startActivity(new Intent(AllCoursesListActivity.this, APTAttendanceActivity.class));
                        finish();
                        break;
                    case 7:
                        startActivity(new Intent(AllCoursesListActivity.this, AcaGradesActivity.class));
                        finish();
                        break;
                    case 8:
                        startActivity(new Intent(AllCoursesListActivity.this, AdviserDetailsActivity.class));
                        finish();
                        break;
                    case 9:
                        startActivity(new Intent(AllCoursesListActivity.this, ProfileActivity.class));
                        finish();
                        break;
                    case 10:
                        startActivity(new Intent(AllCoursesListActivity.this, OptionsActivity.class));
                        finish();
                        break;
                }

            }
        });

    }

    private void setupDrawer() {

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("VITco");
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_options:

                startActivity(new Intent(AllCoursesListActivity.this, OptionsActivity.class));
                break;

            case R.id.action_feedback:

                // Calling doorbell
                new Doorbell(AllCoursesListActivity.this, 2159, "apBxZZVQyXmXsqAdTNhpLivrqwsjsjOljvvuu7wYGofddwEvBFEvULqJfshdN0zn").show();
                break;

            case R.id.action_logout:

                SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                SharedPreferences.Editor ed = sp.edit();
                ed.clear();
                ed.apply();

                AcaGradesTable.deleteAll(AcaGradesTable.class);
                APTAttendanceTable.deleteAll(APTAttendanceTable.class);
                AttendanceDetailedTable.deleteAll(AttendanceDetailedTable.class);
                AttendanceMiniTable.deleteAll(AttendanceMiniTable.class);
                CoursesTable.deleteAll(CoursesTable.class);
                ImagesTable.deleteAll(ImagesTable.class);
                LinksTable.deleteAll(LinksTable.class);
                MarksTable.deleteAll(MarksTable.class);
                MessagesTable.deleteAll(MessagesTable.class);
                OtherDetailsTable.deleteAll(OtherDetailsTable.class);
                SpotlightTable.deleteAll(SpotlightTable.class);
                TimeTableTable.deleteAll(TimeTableTable.class);
                UpdatedLinksTable.deleteAll(UpdatedLinksTable.class);

                startActivity(new Intent(AllCoursesListActivity.this, LoginActivity.class));
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }




}
