package co.mrl.vitco.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import co.mrl.vitco.R;
import co.mrl.vitco.db_tables.CoursesTable;
import co.mrl.vitco.db_tables.TimeTableTable;

/**
 * Created by MrL on 14/1/16.
 */
public class WidgetCourse extends AppWidgetProvider {

    public String TAG = "Widget TAG";
    public String wid_code, wid_time, wid_room_no, wid_name;
    public String[] timings = {"8 am", "9 am", "10 am", "11 am", "12 pm",
            "1 pm", "2 pm", "3 pm", "4 pm", "5 pm", "6 pm", "7 pm", "7 pm", "8 pm"};

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        for (int currentWidgetId : appWidgetIds) {

            Log.i(TAG, "Updated Widget");
            SharedPreferences sp = context.getSharedPreferences("VITco Prefs", Context.MODE_PRIVATE);

            if (sp.getString("widget_update", "").equals("") || sp.getString("widget_update", "").equals("0")) {
                Intent intent = new Intent(context, WidgetCourse.class);
                intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                        20, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                // Refresh widget every 2 min
                AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                alarm.cancel(pendingIntent);
                alarm.setRepeating(AlarmManager.RTC_WAKEUP,
                        Calendar.getInstance().getTimeInMillis() + 1000,
                        1000 * 60 * 5,
                        pendingIntent);

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("widget_update", "1");
                editor.apply();
            }

            if (sp.getString("reg_no", "").equals("")) {

                RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_course);
                views.setTextViewText(R.id.wid_code, "Log ");
                views.setTextViewText(R.id.wid_time, "In");
                views.setTextViewText(R.id.wid_room_no, "Please");
                views.setTextViewText(R.id.wid_name, "");

                appWidgetManager.updateAppWidget(currentWidgetId, views);

            } else {

                set_wid_class_details();

                RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_course);
                views.setTextViewText(R.id.wid_code, wid_code);
                views.setTextViewText(R.id.wid_time, wid_time);
                views.setTextViewText(R.id.wid_room_no, wid_room_no);
                views.setTextViewText(R.id.wid_name, wid_name);

                appWidgetManager.updateAppWidget(currentWidgetId, views);
                //Toast.makeText(context, "Updated + " + currentWidgetId, Toast.LENGTH_SHORT).show();

            }
        }
    }

    public void set_wid_class_details() {

        String[] days_of_week = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
        Map<Integer, String[]> time_course_map = new HashMap<>();
        Calendar cal = Calendar.getInstance();

        // get today's day
        int day_num = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            wid_code = "for ";
            wid_time = "today";
            wid_room_no = "Done";
            wid_name = "";
            return;
        }

        String day_name = days_of_week[day_num];

        // get today's timetable
        Iterator<TimeTableTable> retrieve_day_sch = TimeTableTable
                .find(TimeTableTable.class, "day=?", day_name)
                .iterator();

        int ind = 0;
        while (retrieve_day_sch.hasNext()) {
            TimeTableTable curr_ = retrieve_day_sch.next();
            String[] slot_data = {curr_.course_code, curr_.room_no, timings[ind]};
            time_course_map.put(ind, slot_data);
            ind++;
        }

        // get index in timing array of current time
        int today_ind = get_hour_str(cal);
        if (today_ind == -3) {
            wid_code = "for ";
            wid_time = "today";
            wid_room_no = "Done";
            wid_name = "";
            return;
        }

        // get corr data from map
        int nxt_slot = today_ind + 1;
        // iter till u get (course != '==')
        while (nxt_slot < time_course_map.size()) {
            if (time_course_map.get(nxt_slot)[0].equals("==") || time_course_map.get(nxt_slot)[0].equals("SEM000")) {
                nxt_slot++;
            } else {
                // when found show data
                wid_code = time_course_map.get(nxt_slot)[0];
                wid_room_no = time_course_map.get(nxt_slot)[1];
                wid_time = time_course_map.get(nxt_slot)[2];
                List<CoursesTable> ct = CoursesTable.find(CoursesTable.class, "coursecode=?", wid_code);
                wid_name = ct.get(0).course_name;
                return;
            }
        }
        // if nothing found show done for day
        wid_code = "for ";
        wid_time = " today";
        wid_room_no = "Done";
        wid_name = "";

    }

    public int get_hour_str(Calendar cal) {

        int hour = cal.get(Calendar.HOUR);
        // set it using timings []
        if (cal.get(Calendar.AM_PM) == Calendar.AM) {
            if (hour == 12)
                return -3;
            else if (hour < 8)
                return -1;
            else
                return hour - 8;

        } else {
            if (hour >= 9)
                return -3;
            else if (hour == 12)
                return 4;
            else
                return hour + 4;
        }
    }

}
