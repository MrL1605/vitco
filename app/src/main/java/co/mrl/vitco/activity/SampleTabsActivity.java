package co.mrl.vitco.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.Iterator;

import butterknife.ButterKnife;
import co.mrl.vitco.R;
import co.mrl.vitco.db_tables.CoursesTable;

/**
 * Created by Lalit Umbarkar on 23/10/15.
 */
public class SampleTabsActivity extends AppCompatActivity {

    String mActivityTitle;
//    ExpandablePanel panel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sample_expandable);
        ButterKnife.inject(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Tabs Example");


        //set_dp_photo();
        mActivityTitle = getTitle().toString();


        Iterator<CoursesTable> courses = CoursesTable.findAll(CoursesTable.class);
        String[] tmp_course_codes = new String[30];
        String[] tmp_course_nbr = new String[30];
        String[] tmp_course_names = new String[30];
        String[] tmp_faculty_names = new String[30];
        int i = 0;
        while (courses.hasNext()) {
            CoursesTable temp = courses.next();
            tmp_course_names[i] = temp.course_name;
            tmp_course_codes[i] = temp.course_code;
            tmp_course_nbr[i] = temp.class_nbr + "";
            tmp_faculty_names[i] = temp.faculty_name;
            i += 1;
        }
        String[] course_names = new String[i];
        String[] course_codes = new String[i];
        String[] faculty_names = new String[i];
        String[] course_nbr = new String[i];

        for (int j = 0; j < i; j++) {
            course_names[j] = tmp_course_names[j];
            course_codes[j] = tmp_course_codes[j];
            faculty_names[j] = tmp_faculty_names[j];
            course_nbr[j] = tmp_course_nbr[j];
        }

        // Sort the courses by their names.
        for (int j = 0; j < course_names.length; j++) {
            for (i = j + 1; i < course_names.length; i++) {
                if (course_names[i].compareToIgnoreCase(course_names[j]) < 0) {
                    String temp = course_names[j];
                    String temp_code = course_codes[j];
                    String temp_nbr = course_nbr[j];
                    String temp_f_name = faculty_names[j];
                    course_names[j] = course_names[i];
                    course_codes[j] = course_codes[i];
                    course_nbr[j] = course_nbr[i];
                    faculty_names[j] = faculty_names[i];
                    course_names[i] = temp;
                    course_codes[i] = temp_code;
                    faculty_names[i] = temp_f_name;
                    course_nbr[i] = temp_nbr;
                }
            }
        }

//        ListView lv = (ListView) findViewById(R.id.exp_lv);
//        lv.setAdapter(new ExpandableViewAdapter(this, course_names));

    }
}
