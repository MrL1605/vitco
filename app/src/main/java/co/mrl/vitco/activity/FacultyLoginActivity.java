package co.mrl.vitco.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pkmmte.view.CircularImageView;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.mrl.vitco.R;
import co.mrl.vitco.model.CaptchaParser;

/**
 * Created by root on 20/10/15.
 */
public class FacultyLoginActivity extends AppCompatActivity {

    public static final String SPOTLIGHT_URL = "https://academics.vit.ac.in/student/stud_home.asp";
    public static final String ACCESS_URL = "https://academics.vit.ac.in/faculty/fac_login.asp";
    public static final String SUBMIT_URL = "https://academics.vit.ac.in/student/stud_login_submit.asp";
    public static final String CAPTCHA_URL = "https://academics.vit.ac.in/student/captcha.asp";
    public static final String PATH_NAME = "/VITco/Images/";
    ArrayAdapter<String> mAdapter;
    ActionBarDrawerToggle mDrawerToggle;
    Map<String, String> cookies;
    Map<String, String> formFields;
    ProgressDialog pd;
    String mActivityTitle;
    @InjectView(R.id.nav_list)
    ListView mDrawerList;
    @InjectView(R.id.emp_id_et)
    EditText emp_id_et;
    @InjectView(R.id.password_et)
    EditText pass_et;
    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    //@Optional
    //@InjectView(R.id.profile_dp)
    CircularImageView circular_profile_dp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faculty_login_activity);
        ButterKnife.inject(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Faculty Login");

        //set_dp_photo();
        mActivityTitle = getTitle().toString();
        addDrawerItems();
        setupDrawer();


        cookies = new HashMap<>();
        formFields = new HashMap<>();

        /*
        srl = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        srl.setOnRefreshListener(this);
        srl.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        */

        // In case certificate are Expired.
        setTrustAllCerts();


        findViewById(R.id.testing_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                // String reg_no = sp.getString("reg_no", "");
                // String password = sp.getString("password", "");
                String emp_id = emp_id_et.getText().toString();
                String password = pass_et.getText().toString();
                Log.i("Click", " Got pass " + emp_id);
                new AcademicsLogin().execute(emp_id, password);
                pd = ProgressDialog.show(FacultyLoginActivity.this, "", "Logging in Faculty Login", true);

            }
        });

    }

    public void setTrustAllCerts() {
        /**
         * To Bypass SSL Certificate Check. In case the Academics Portal Certificate is expired.
         */
        try {
            // Create a custom Trust Manager
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };


            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                        public boolean verify(String urlHostName, SSLSession session) {
                            return true;
                        }
                    });
        } catch (Exception e) {
            //We can not recover from this exception.
            //e.printStackTrace();
        }
    }

    public class AcademicsLogin extends AsyncTask<String, String, String> {
        /**
         * Does the Academics Login requires Reg No and Password as arguments
         */

        Bitmap bm;
        Connection.Response resp;
        String cap_val = "";

        @Override
        protected void onPreExecute() {
            /**
             * On Pre is executed before Background. Count Down Timer is scheduled so that
             * If Login is not successful until 1 min. Async task is cancelled.
             */
            super.onPreExecute();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
            Log.i("Login", " PreExecute");

        }

        @Override
        protected String doInBackground(String... strings) {
            /**
             * Returns  "Success" -> Login successfully.
             *          "Failed" -> UpdatedLinksTable could not be updated successfully.
             *          "Wrong Password" -> When user's Reg No. and Passwords do not match.
             * Does Login for academics.
             * Stores cookies returned in cookies variable.
             */

            String emp_id = strings[0];
            String password = strings[1];

            try {
                Log.i("Login", " 0%");
                resp = Jsoup.connect(ACCESS_URL)
                        .timeout(100000)
                        .method(Connection.Method.GET)
                        .userAgent("Mozilla/5.0").execute();

                // Get request to login form of Academics Page, and store cookies, as well as their form fields.
                // Cookies are important. We will use them everywhere.

                Log.i("Login", "10%");
                cookies = resp.cookies();

                Elements fields = resp.parse().select("form input");
                formFields = new HashMap<>();
                for (Element field : fields) {
                    if (!field.attr("name").equals("")) {
                        formFields.put(field.attr("name"), field.attr("value"));
                    }
                }

                // Post request to Captcha page to retrieve Captcha Image, using cookies.
                Log.i("Login", "20%");

                Connection.Response resultImageResponse = Jsoup.connect(CAPTCHA_URL)
                        .cookies(cookies)
                        .ignoreContentType(true)
                        .method(Connection.Method.GET).timeout(100000).execute();

                Log.i("Login", "30%");
                byte[] bitmap_Data = resultImageResponse.bodyAsBytes();
                bm = BitmapFactory.decodeByteArray(bitmap_Data, 0, bitmap_Data.length);

                // Send this Bitmap to CaptchaParser.java to parse it,
                // It returns captcha value as XXXXXX.

                CaptchaParser cp = new CaptchaParser();
                cp.setBm(bm);
                cap_val = cp.getCaptcha();

                // Enter the details in form fields
                Log.i("Login", "40%");

                formFields.put("empid", emp_id);
                formFields.put("pswd", password);
                formFields.put("vrfcd", cap_val);

                // Get Reg No and Password and make login Login using Post request with cookies.
                Log.i("Login", "40%");

                Connection.Response access_url_resp = Jsoup.connect(SUBMIT_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                                // Extra headers
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Host", "academics.vit.ac.in")
                        .header("Connection", "keep-alive")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_login.asp")
                        .cookies(cookies)
                        .followRedirects(true)
                        .data(formFields)
                        .timeout(100000)
                        .method(Connection.Method.POST).execute();

                // Check if logged in by checking if cookies returned has reg number in it.
                // If Reg No was there, then Academics server accepted Login. Otherwise throw Exception.
                Log.i("Login", "45%");

                boolean logged_in = false;
                Map<String, String> temp = access_url_resp.cookies();
                for (String key : temp.keySet()) {
                    if (key.equals("logattempid")) {
                        if (temp.get(key).equals(emp_id)) {
                            logged_in = true;
                        }
                    }
                    cookies.put(key, temp.get(key));
                }

                if (logged_in) {
                    // not to store currently
                    Log.i("Login ", "completed");
                    /*
                    SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.clear();
                    editor.putString("emp_id", emp_id);
                    editor.putString("password", password);
                    editor.apply();
                    */
                } else {
                    throw new WrongPassword();
                }
                Log.i("Login", "100%");


            } catch (WrongPassword wp) {
                // Password Wrong Caught here.
                //wp.printStackTrace();
                return "Wrong Password";
            } catch (Exception e) {
                // Any Connection Error Caught here.
                //e.printStackTrace();
                return "Network Error";
            }
            return "Success";
        }

        @Override
        protected void onPostExecute(String s) {
            /**
             * Next step, Retrieve all data from all courses.
             */

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            TextView test_tv = (TextView) findViewById(R.id.testing_tv);
            test_tv.setText("Login " + s);
            switch (s) {
                case "Success":
                    new GetFacultyAttendanceData().execute();
                    break;
                case "Network Error":
//                    srl.setRefreshing(false);
                    pd.dismiss();
                    Toast.makeText(FacultyLoginActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
                    break;
                case "Wrong Password":
//                    srl.setRefreshing(false);
                    pd.dismiss();
                    Toast.makeText(FacultyLoginActivity.this, "Please Check Credentials", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    public class GetFacultyAttendanceData extends AsyncTask<String, String, String> {

        Connection.Response resp;

        /**
         * Does the extraction of Spotlight data
         *
         * @return "Success" -> Profile data extracted.
         * "Failed" -> Unable to extract data.
         */
        @Override
        protected String doInBackground(String... param) {

            try {

                String data = "";
                Log.i("GET Profile", "10%");
/*
                resp = Jsoup.connect(SPOTLIGHT_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                        .cookies(cookies)
                        .method(Connection.Method.GET).execute();

                Document doc = resp.parse();
                Element spotlight_marquee = doc.select("marquee").first();
                Elements all_rows = spotlight_marquee.select("td");
                for (Element row : all_rows){
                    data += row.text() + " || ";
                }

                Element messages_marquee = doc.select("marquee").get(1);
                all_rows = messages_marquee.select("td");
                data += ":=>  Messages :=>  ";
                for (Element row : all_rows){
                    data += row.text() + " || ";
                }
*/
                return "No data yet";

            } catch (Exception e) {
                e.printStackTrace();
                return "Failed";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            TextView test_tv = (TextView) findViewById(R.id.testing_tv);
            test_tv.setText(s);
            pd.dismiss();
//            srl.setRefreshing(false);

        }
    }

    public class WrongPassword extends Exception {
        public WrongPassword() {
        }

        @Override
        public String toString() {
            return "Wrong Password Entered";
        }
    }


    private void set_dp_photo() {

        File root = android.os.Environment.getExternalStorageDirectory();
        File file = new File(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
        if (file.exists()) {
            circular_profile_dp.setSelectorStrokeWidth(10);
            circular_profile_dp.addShadow();
            Bitmap bm = BitmapFactory.decodeFile(root.getAbsolutePath() + PATH_NAME + "profile_dp.bmp");
            bm = Bitmap.createBitmap(bm, 0, 10, bm.getWidth(), bm.getWidth());
            circular_profile_dp.setImageBitmap(bm);
        }
    }

    private void addDrawerItems() {
        String[] osArray = { "TimeTable", "Marks", "SpotLight", "Course Page", "Faculty Login", "Attendance", "APT Attendance", "Academic Grades", "Adviser", "Profile", "Settings"};
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(FacultyLoginActivity.this, TimeTableActivity.class));
                        finish();
                        break;
                    case 1:
                        startActivity(new Intent(FacultyLoginActivity.this, MarksActivity.class));
                        finish();
                        break;
                    case 2:
                        startActivity(new Intent(FacultyLoginActivity.this, SpotlightActivity.class));
                        finish();
                        break;
                    case 3:
                        startActivity(new Intent(FacultyLoginActivity.this, AllCoursesListActivity.class));
                        finish();
                        break;
                    case 4:
                        startActivity(new Intent(FacultyLoginActivity.this, MainPageActivity.class));
                        finish();
                        break;
                    case 5:
                        startActivity(new Intent(FacultyLoginActivity.this, AttendanceActivity.class));
                        finish();
                        break;
                    case 6:
                        startActivity(new Intent(FacultyLoginActivity.this, APTAttendanceActivity.class));
                        finish();
                        break;
                    case 7:
                        startActivity(new Intent(FacultyLoginActivity.this, AcaGradesActivity.class));
                        finish();
                        break;
                    case 8:
                        startActivity(new Intent(FacultyLoginActivity.this, AdviserDetailsActivity.class));
                        finish();
                        break;
                    case 9:
                        startActivity(new Intent(FacultyLoginActivity.this, ProfileActivity.class));
                        finish();
                        break;
                    case 10:
                        startActivity(new Intent(FacultyLoginActivity.this, OptionsActivity.class));
                        finish();
                        break;
                }
            }
        });
    }

    private void setupDrawer() {

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("VITco");
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_options) {
            return true;
        }

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

}
