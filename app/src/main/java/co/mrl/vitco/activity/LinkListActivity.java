package co.mrl.vitco.activity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.Toast;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.mrl.vitco.R;
import co.mrl.vitco.adapter.LinkListAdapter;
import co.mrl.vitco.db_tables.CoursesTable;
import co.mrl.vitco.db_tables.LinksTable;
import co.mrl.vitco.model.CaptchaParser;

/**
 * Created by lalit on 3/9/15.
 */

@SuppressWarnings("deprecation")
public class LinkListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static final String ACCESS_URL = "https://academics.vit.ac.in/student/stud_login.asp";
    public static final String SUBMIT_URL = "https://academics.vit.ac.in/student/stud_login_submit.asp";
    public static final String CAPTCHA_URL = "https://academics.vit.ac.in/student/captcha.asp";
    public static final String COURSE_PAGE_URL = "https://academics.vit.ac.in/student/coursepage_view3.asp";
    Map<String, String> cookies;
    Map<String, String> formFields;
    ProgressDialog pd;
    @InjectView(R.id.links_lv)
    ListView links_list_view;
    String curr_cpc;
    @InjectView(R.id.refresh_layout)
    SwipeRefreshLayout srl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.link_list_activity);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        ButterKnife.inject(this);

        setTrustAllCerts();

        String class_nbr = getIntent().getExtras().getString("class_nbr");
        List<CoursesTable> temp_course = CoursesTable.find(CoursesTable.class, "classnbr=?", class_nbr);
        curr_cpc = temp_course.get(0).class_plan_code;
        setTitle(temp_course.get(0).course_name);

        srl.setOnRefreshListener(this);
        srl.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        List<LinksTable> tmp_material_list = LinksTable.find(LinksTable.class, "coursecpc = ?", curr_cpc);
        if (tmp_material_list.size() == 0) {
            new AcademicsLogin().execute();
        } else {
            get_urls(curr_cpc);
        }
    }

    public void get_urls(String curr_cpc_val) {

        String[] material_url, material_names;
        boolean[] is_downloading;
        List<LinksTable> courses = LinksTable.find(LinksTable.class, "coursecpc = ?", curr_cpc_val);

        material_url = new String[courses.size()];
        material_names = new String[courses.size()];
        is_downloading = new boolean[courses.size()];

        for (int j = 0; j < courses.size(); j++) {
            material_names[j] = courses.get(j).material_name;
            material_url[j] = courses.get(j).material_link;
            is_downloading[j] = courses.get(j).is_downloading;
        }

        links_list_view = (ListView) findViewById(R.id.links_lv);
        links_list_view.setAdapter(new LinkListAdapter(this, curr_cpc_val, material_names, material_url, is_downloading));

    }

    @Override
    public void onRefresh() {

        SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
        String reg_no = sp.getString("reg_no", "");
        String password = sp.getString("password", "");
        new AcademicsLogin().execute(reg_no, password);
    }

    public void setTrustAllCerts() {
        /**
         * To Bypass SSL Certificate Check. In case the Academics Portal Certificate is expired.
         */
        try {
            // Create a custom Trust Manager
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };


            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                        public boolean verify(String urlHostName, SSLSession session) {
                            return true;
                        }
                    });
        } catch (Exception e) {
            //We can not recover from this exception.
            //e.printStackTrace();
        }
    }

    private class AcademicsLogin extends AsyncTask<String, String, String> {

        Bitmap bm;
        Connection.Response resp;
        String cap_val = "";

        @Override
        protected void onPreExecute() {
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
            pd = ProgressDialog.show(LinkListActivity.this, "", "Loading Links", true);

        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                resp = Jsoup.connect(ACCESS_URL)
                        .timeout(300000)
                        .method(Connection.Method.GET)
                        .userAgent("Mozilla/5.0").execute();

                cookies = resp.cookies();

                Elements fields = resp.parse().select("form input");
                formFields = new HashMap<>();
                for (Element field : fields) {
                    if (!field.attr("name").equals("")) {
                        formFields.put(field.attr("name"), field.attr("value"));
                    }
                }

                // Post request to Captcha page to retrieve Captcha Image, using cookies.

                Connection.Response resultImageResponse = Jsoup.connect(CAPTCHA_URL)
                        .cookies(cookies)
                        .ignoreContentType(true)
                        .method(Connection.Method.GET).timeout(30000).execute();

                byte[] bitmap_Data = resultImageResponse.bodyAsBytes();
                bm = BitmapFactory.decodeByteArray(bitmap_Data, 0, bitmap_Data.length);

                // Send this Bitmap to CaptchaParser.java to parse it,
                // It returns captcha value as XXXXXX.

                CaptchaParser cp = new CaptchaParser();
                cp.setBm(bm);
                cap_val = cp.getCaptcha();

                // Get Reg No and Password  from shared prefs
                SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                String reg_no = sp.getString("reg_no", "");
                String password = sp.getString("password", "");

                if (!(reg_no.equals("") || password.equals(""))) {
                    formFields.put("regno", reg_no);
                    formFields.put("passwd", password);
                    formFields.put("vrfcd", cap_val);
                }

                Connection.Response access_url_resp = Jsoup.connect(SUBMIT_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                                // Extra headers
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Host", "academics.vit.ac.in")
                        .header("Connection", "keep-alive")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_login.asp")
                        .cookies(cookies)
                        .followRedirects(true)
                        .data(formFields)
                        .timeout(300000)
                        .method(Connection.Method.POST).execute();


                Map<String, String> temp = access_url_resp.cookies();
                for (String key : temp.keySet()) {
                    cookies.put(key, temp.get(key));
                }

            } catch (Exception e) {
                //e.printStackTrace();
                return "Failed";
            }
            return "Success";
        }

        @Override
        protected void onPostExecute(String s) {

            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            if (s.equals("Success") && !cap_val.equals("")) {
                new CoursePageRetriever().execute(curr_cpc);
            } else {
                srl.setRefreshing(false);
                pd.dismiss();
                Toast.makeText(LinkListActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
            }

        }
    }

    private class CoursePageRetriever extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                formFields.clear();
                formFields.put("sem", "FS");
                formFields.put("crsplancode", strings[0]);
                formFields.put("crpnvwcmd", "View");

                Connection.Response course_page_resp = Jsoup.connect(COURSE_PAGE_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Connection", "keep-alive")
                        .header("Host", "academics.vit.ac.in")
                        .header("Referer", "https://academics.vit.ac.in/student/coursepage_view.asp?sem=FS&crs=&slt=&fac=")
                        .header("DNT", "1")
                        .cookies(cookies)
                        .data(formFields)
                        .ignoreContentType(false)
                        .timeout(300000).followRedirects(true)
                        .method(Connection.Method.POST).execute();

                if (course_page_resp.statusCode() == 200) {
                    course_page_resp = Jsoup.connect(COURSE_PAGE_URL)
                            .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                            .header("Accept-Encoding", "gzip, deflate")
                            .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                            .header("Connection", "keep-alive")
                            .header("Host", "academics.vit.ac.in")
                            .header("Referer", "https://academics.vit.ac.in/student/coursepage_view.asp?sem=FS&crs=&slt=&fac=")
                            .header("DNT", "1")
                            .cookies(cookies)
                            .data(formFields)
                            .ignoreContentType(false)
                            .timeout(300000).followRedirects(true)
                            .method(Connection.Method.POST).execute();
                }

                Document doc = course_page_resp.parse();
                Elements links = doc.select("a[target=\"_blank\"]");
                List<LinksTable> remove_old = LinksTable.find(LinksTable.class, "coursecpc = ?", strings[0]);
                for (int i = 0; i < remove_old.size(); i++) {
                    remove_old.get(i).delete();
                }
                String material_name = "";
                for (Element link : links) {
                    String material_url = link.attr("href");
                    try {
                        if (material_url.split("[A-Z]{2,4}[0-9]{2}_").length >= 2) {
                            String[] tmp = material_url.split("[A-Z]{2,4}[0-9]{2}_");
                            material_name = tmp[tmp.length - 1];
                        } else {
                            material_name = material_url.split(strings[0])[1];
                        }
                    } catch (ArrayIndexOutOfBoundsException aiob) {
                        //aiob.printStackTrace();
                    }
                    if (!(material_name.equals("") || material_url.equals(""))) {
                        LinksTable add_new = new LinksTable(strings[0], material_url, material_name);
                        add_new.save();
                    }
                }
            } catch (Exception e) {
                //e.printStackTrace();
                return "Failed";
            }
            return strings[0];
        }

        @Override
        protected void onPostExecute(String s) {

            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            pd.dismiss();
            srl.setRefreshing(false);
            if (!s.equals("Failed")) {
                get_urls(s);
            } else {
                Toast.makeText(LinkListActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
            }
        }
    }

}
