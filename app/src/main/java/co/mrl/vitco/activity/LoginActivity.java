package co.mrl.vitco.activity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.john.waveview.WaveView;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.mrl.vitco.R;
import co.mrl.vitco.db_tables.CoursesTable;
import co.mrl.vitco.model.CaptchaParser;
import co.mrl.vitco.model.UpdateService;

@SuppressWarnings("deprecation")
public class LoginActivity extends Activity {

    public static final String ACCESS_URL = "https://academics.vit.ac.in/student/stud_login.asp";
    public static final String SUBMIT_URL = "https://academics.vit.ac.in/student/stud_login_submit.asp";
    public static final String CAPTCHA_URL = "https://academics.vit.ac.in/student/captcha.asp";
    public static final String TIMETABLE_URL = "https://academics.vit.ac.in/student/timetable_ws.asp";
    public static final String ITER_COURSE_PAGE_URL = "https://academics.vit.ac.in/student/coursepage_view.asp?sem=WS&slt=&fac=&crs=";
    private static final String REG_NO_PATTERN = "^[0-9]{2}[A-Z]{3}[0-9]{4}$";
    @InjectView(R.id.wave_view)
    WaveView wv;
    @InjectView(R.id.reg_no_et)
    EditText reg_no_et;
    @InjectView(R.id.circular_progress_bar)
    ProgressBar p_bar;
    @InjectView(R.id.water_tv)
    TextView water_tv;
    @InjectView(R.id.pass_et)
    EditText pass_et;
    @InjectView(R.id.register_bt)
    Button reg_bt;
    Map<String, String> cookies;
    Map<String, String> formFields;
    Map<String, String> code_nbr;
    Map<String, String> code_name;
    Map<String, String> code_f_name;
    int curr_wave_value = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /**
         * Main Screen if not Logged in to VITco. OnCreate is called at first.
         * Variables are Initialized.
         */

        super.onCreate(savedInstanceState);

        // Check if Logged in
        SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
        if (!(sp.getString("reg_no", "").equals(""))) {
            // If yes redirect to Curses Page
            startActivity(new Intent(LoginActivity.this, TimeTableActivity.class));
            finish();
        }
        // Set content Layout to Login Screen
        setContentView(R.layout.login_activity);

        ButterKnife.inject(this);
        setTrustAllCerts();

        cookies = new HashMap<>();
        formFields = new HashMap<>();
        code_nbr = new HashMap<>();
        code_name = new HashMap<>();
        code_f_name = new HashMap<>();

        // Init wave to 0
        WaveChangeListener(0);

        reg_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String reg_no = reg_no_et.getText().toString();
                String password = pass_et.getText().toString();
                WaveChangeListener(10);

                Matcher reg_matcher = Pattern.compile(REG_NO_PATTERN).matcher(reg_no);
                if (reg_matcher.matches()) {
                    // Check if Reg No pattern Matches
                    new AcademicsLogin().execute(reg_no, password);

                } else {
                    // Else send a toast and set wave to 0.
                    WaveChangeListener(0);
                    Toast.makeText(LoginActivity.this, "Please Enter Valid \nRegistration Number", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void WaveChangeListener(int f_val) {
        /**
         * Function takes val to be set to wave as parameter
         * If f_val is 0, progress bar and ext view is not displayed anymore
         * Function can be called from Async class too.
         */

        if (f_val >= 100) {
            // In case if set more than 100, To avoid Error
            curr_wave_value = 100;
        } else if (f_val < 0) {
            // In case if set less than 0, To avoid Error
            curr_wave_value = 0;
        } else {
            // Else
            curr_wave_value = f_val;
        }
        // Run on UI So that function can be called from both Async Class and Main Thread.

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (curr_wave_value == 0) {
                    // Set things to normal if set to 0
                    reg_bt.setEnabled(true);
                    p_bar.setVisibility(View.GONE);
                    water_tv.setVisibility(View.GONE);
                } else if (reg_bt.isEnabled()) {
                    // Set disabled things if not 0
                    reg_bt.setEnabled(false);
                    p_bar.setVisibility(View.VISIBLE);
                    water_tv.setVisibility(View.VISIBLE);
                }
                wv.setProgress(curr_wave_value);
            }
        });
    }

    public void setTrustAllCerts() {
        /**
         * To Bypass SSL Certificate Check. In case the Academics Portal Certificate is expired.
         */
        try {
            // Create a custom Trust Manager
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };


            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                        public boolean verify(String urlHostName, SSLSession session) {
                            return true;
                        }
                    });
        } catch (Exception e) {
            //We can not recover from this exception.
            //e.printStackTrace();
        }
    }

    public class AcademicsLogin extends AsyncTask<String, String, String> {
        /**
         * Does the Academics Login requires Reg No and Password as arguments
         */

        Bitmap bm;
        Connection.Response resp;
        String cap_val = "";

        @Override
        protected void onPreExecute() {
            /**
             * On Pre is executed before Background.
             * Sets the orientation.
             */
            super.onPreExecute();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        }

        @Override
        protected String doInBackground(String... strings) {
            /**
             * Returns  "Success" -> Login successfully.
             *          "Failed" -> UpdatedLinksTable could not be updated successfully.
             *          "Wrong Password" -> When user's Reg No. and Passwords do not match.
             * Does Login for academics.
             * Stores cookies returned in cookies variable.
             */

            String reg_no = strings[0];
            String password = strings[1];

            try {
                resp = Jsoup.connect(ACCESS_URL)
                        .timeout(300000)
                        .method(Connection.Method.GET)
                        .userAgent("Mozilla/5.0").execute();

                // Get request to login form of Academics Page, and store cookies, as well as their form fields.
                // Cookies are important. We will use them everywhere.
                // Set wave to 20 %

                WaveChangeListener(20);

                cookies = resp.cookies();
                Elements fields = resp.parse().select("form input");
                formFields = new HashMap<>();
                for (Element field : fields) {
                    if (!field.attr("name").equals("")) {
                        formFields.put(field.attr("name"), field.attr("value"));
                    }
                }

                // Post request to Captcha page to retrieve Captcha Image, using cookies.

                Connection.Response resultImageResponse = Jsoup.connect(CAPTCHA_URL)
                        .cookies(cookies)
                        .ignoreContentType(true)
                        .method(Connection.Method.GET).timeout(30000).execute();

                WaveChangeListener(30);

                byte[] bitmap_Data = resultImageResponse.bodyAsBytes();
                bm = BitmapFactory.decodeByteArray(bitmap_Data, 0, bitmap_Data.length);

                // Send this Bitmap to CaptchaParser.java to parse it,
                // It returns captcha value as XXXXXX.

                CaptchaParser cp = new CaptchaParser();
                cp.setBm(bm);
                cap_val = cp.getCaptcha();

                // Enter values in form field to submit

                formFields.put("regno", reg_no);
                formFields.put("passwd", password);
                formFields.put("vrfcd", cap_val);

                Connection.Response access_url_resp = Jsoup.connect(SUBMIT_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                                // Extra headers
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Host", "academics.vit.ac.in")
                        .header("Connection", "keep-alive")
                        .header("DNT", "1")
                        .header("Referer", "https://academics.vit.ac.in/student/stud_login.asp")
                        .cookies(cookies)
                        .followRedirects(true)
                        .data(formFields)
                        .timeout(300000)
                        .method(Connection.Method.POST).execute();

                WaveChangeListener(55);

                // Get Reg No and Password and make login Login using Post request with cookies.

                boolean logged_in = false;
                Map<String, String> temp = access_url_resp.cookies();
                for (String key : temp.keySet()) {
                    if (key.equals("logstudregno")) {
                        if (temp.get(key).equals(reg_no)) {
                            logged_in = true;
                        }
                    }
                    cookies.put(key, temp.get(key));
                }

                // Check if logged in by checking if cookies returned has reg number in it.
                // If Reg No was there, then Academics server accepted Login. Otherwise throw Exception.

                if (logged_in) {
                    SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.clear();
                    editor.putString("reg_no", reg_no);
                    editor.putString("password", password);
                    editor.putString("alarm_set", "1");
                    editor.apply();

                    // Set the Alarm by default.

                    Calendar cal = Calendar.getInstance();
                    Intent intent = new Intent(LoginActivity.this, UpdateService.class);
                    PendingIntent p_intent = PendingIntent.getService(LoginActivity.this, 0, intent, 0);
                    AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarm.cancel(p_intent);
                    alarm.setRepeating(AlarmManager.RTC_WAKEUP,
                            cal.getTimeInMillis() + 1000 * 60 * 3, AlarmManager.INTERVAL_HOUR * 3, p_intent);

                } else {
                    throw new WrongPassword();
                }


            } catch (WrongPassword wp) {
                WaveChangeListener(0);
                //wp.printStackTrace();
                return "Wrong Password";
            } catch (Exception e) {
                WaveChangeListener(0);
                //e.printStackTrace();
                return "Network Error";
            }
            return "Success";
        }

        @Override
        protected void onPostExecute(String s) {
            /**
             * Next step, Retrieve all data from all courses.
             */

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            switch (s) {
                case "Success":
                    new ClassNbrListExtractor().execute();
                    break;
                case "Network Error":
                    Toast.makeText(LoginActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
                    break;
                case "Wrong Password":
                    Toast.makeText(LoginActivity.this, "Please Check Credentials", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    public class ClassNbrListExtractor extends AsyncTask<String, String, String> {
        /**
         * Extracts all course codes from timetable.
         */

        @Override
        protected void onPreExecute() {
            /**
             * On Pre is executed before Background.
             * Sets the screen orientation.
             */

            super.onPreExecute();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        }

        @Override
        protected String doInBackground(String... strings) {
            /**
             * Returns  "Success" -> CoursesTable updated with new links successfully.
             *          "Failed" -> CoursesLinksTable could not be updated successfully.
             * Retrieves all courses available in Timetable with class nbr to be stored in DB.
             */

            try {
                Connection.Response timetable_rep = Jsoup.connect(TIMETABLE_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Connection", "keep-alive")
                        .header("Host", "academics.vit.ac.in")
                        .header("DNT", "1")
                        .cookies(cookies)
                        .ignoreContentType(false)
                        .timeout(300000).followRedirects(true)
                        .method(Connection.Method.GET).execute();

                WaveChangeListener(70);

                // If first try is Object moved
                // then do second try
                if (timetable_rep.statusCode() == 200) {
                    timetable_rep = Jsoup.connect(TIMETABLE_URL)
                            .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                            .header("Accept-Encoding", "gzip, deflate")
                            .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                            .header("Connection", "keep-alive")
                            .header("Host", "academics.vit.ac.in")
                            .header("DNT", "1")
                            .cookies(cookies)
                            .ignoreContentType(false)
                            .timeout(300000).followRedirects(true)
                            .method(Connection.Method.GET).execute();
                }

                // Get timetable, extract all courses details
                WaveChangeListener(85);
                Document document = timetable_rep.parse();
                Elements all_rows = document.select("tr[bgcolor=\"#EDEADE\"]");
                String sample, sample_code, sample_name, faculty_name;
                for (Element line : all_rows) {
                    Iterator<Element> tds = line.select("font[color=\"green\"]").iterator();
                    boolean test = false;
                    int class_nbr = -1605;
                    sample = "";
                    faculty_name = "";
                    sample_name = "";
                    sample_code = "";

                    try {
                        sample = tds.next().text();
                        class_nbr = Integer.parseInt(sample);
                        if (class_nbr > 999) {
                            sample_code = tds.next().text();
                            sample_name = tds.next().text();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            faculty_name = tds.next().text();
                            test = true;
                        }
                    } catch (Exception e) {
                        test = false;
                    }
                    if (!test) {
                        try {
                            sample = tds.next().text();
                            sample_code = tds.next().text();
                            sample_name = tds.next().text();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            tds.next();
                            faculty_name = tds.next().text();
                            class_nbr = Integer.parseInt(sample);
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                    }
                    if (class_nbr != -1605) {
                        code_nbr.put("" + class_nbr, sample_code);
                        code_f_name.put("" + class_nbr, faculty_name);
                        code_name.put("" + class_nbr, sample_name);
                    }
                }

                WaveChangeListener(90);

            } catch (Exception e) {
                //e.printStackTrace();
                WaveChangeListener(0);
                return "Failed";
            }
            return "Success";
        }

        @Override
        protected void onPostExecute(String s) {
            /**
             * Next Step to Retrieve all course plan codes from Course Page
             */
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            if (s.equals("Success")) {
                new ClassPlanCodeExtraction().execute();
            } else if (s.equals("Failed")) {
                Toast.makeText(LoginActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
            }

        }
    }

    public class ClassPlanCodeExtraction extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            /**
             * Before execute is called.
             * Sets the orientation of scrren.
             */
            super.onPreExecute();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

        }

        @Override
        protected String doInBackground(String... strings) {

            String db_course_code, db_class_nbr, db_f_name, db_course_name;
            CoursesTable.deleteAll(CoursesTable.class);

            try {
                for (String nbr_key : code_nbr.keySet()) {
                    db_class_nbr = nbr_key;
                    db_course_code = code_nbr.get(nbr_key);
                    db_f_name = code_f_name.get(nbr_key);
                    db_course_name = code_name.get(nbr_key);

                    String course_url = ITER_COURSE_PAGE_URL + db_course_code;

                    Connection.Response marks_resp = Jsoup.connect(course_url)
                            .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                            .header("Accept-Encoding", "gzip, deflate")
                            .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                            .header("Connection", "keep-alive")
                            .header("Host", "academics.vit.ac.in")
                            .header("DNT", "1")
                            .cookies(cookies)
                            .ignoreContentType(false)
                            .timeout(300000).followRedirects(true)
                            .method(Connection.Method.GET).execute();

                    Document doc = marks_resp.parse();
                    String course_plan_code, class_nbr;
                    Elements all_rows = doc.select("tr[bgcolor=\"#E1ECF2\"]");

                    for (Element row : all_rows) {
                        class_nbr = row.select("td[width=\"100\"],[align=\"center\"]").eq(1).text();
                        course_plan_code = row.select("input [type=\"hidden\"],[name=\"crsplancode\"]").first().val();
                        if (db_class_nbr.equals(class_nbr)) {
                            CoursesTable new_add = new CoursesTable(course_plan_code, Integer.parseInt(class_nbr), db_course_code, db_f_name, db_course_name);
                            new_add.save();
                        }
                    }
                }

                WaveChangeListener(100);
                startActivity(new Intent(LoginActivity.this, AllCoursesListActivity.class));
                finish();
            } catch (Exception e) {
                //e.printStackTrace();
                WaveChangeListener(0);
                return "Failed";
            }
            return "Success";
        }

        @Override
        protected void onPostExecute(String s) {

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            if (s.equals("Failed")) {
                Toast.makeText(LoginActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
            }

        }
    }

    public class WrongPassword extends Exception {
        public WrongPassword() {
        }

        @Override
        public String toString() {
            return "Wrong Password Entered";
        }
    }

}
