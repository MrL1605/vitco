package co.mrl.vitco.db_tables;

import com.orm.SugarRecord;

/**
 * Created by root on 20/10/15.
 */
public class AttendanceMiniTable extends SugarRecord<AttendanceMiniTable> {

    public int classnbr, attend_classes, total_classes;
    public String per_class, status;

    public AttendanceMiniTable() {
    }

    public AttendanceMiniTable(int classnbr, int attend_classes, int total_classes, String per_class, String status) {
        this.classnbr = classnbr;
        this.attend_classes = attend_classes;
        this.total_classes = total_classes;
        this.per_class = per_class;
        this.status = status;
    }

}
