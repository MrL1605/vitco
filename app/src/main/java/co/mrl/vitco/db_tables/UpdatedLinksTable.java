package co.mrl.vitco.db_tables;

import com.orm.SugarRecord;

/**
 * Created by lalit on 8/9/15.
 */

public class UpdatedLinksTable extends SugarRecord {
    /**
     * Updated when using Update Service in Background
     * Similar to LinksTable
     */

    public String course_cpc, material_link, material_name;
    public boolean is_downloading = false;

    public UpdatedLinksTable() {
    }

    public UpdatedLinksTable(String course_cpc, String material_link, String material_name) {
        this.course_cpc = course_cpc;
        this.material_link = material_link;
        this.material_name = material_name;
    }

}
