package co.mrl.vitco.db_tables;

import com.orm.SugarRecord;

/**
 * Created by root on 21/10/15.
 */
public class MessagesTable extends SugarRecord<MessagesTable> {
    /**
     * Contains the messages data with row_id as their sequence number.
     */

    public int row_id;
    public String data;

    public MessagesTable() {
    }

    public MessagesTable(int row_id, String data) {
        /**
         * Sets all data with its row_id
         */
        this.row_id = row_id;
        this.data = data;
    }

}
