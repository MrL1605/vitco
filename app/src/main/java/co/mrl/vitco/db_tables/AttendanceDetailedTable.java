package co.mrl.vitco.db_tables;

import com.orm.SugarRecord;

/**
 * Created by root on 20/10/15.
 */
public class AttendanceDetailedTable extends SugarRecord<AttendanceDetailedTable> {

    public String slno, date, slot, status, units, reason;
    public int classnbr;

    public AttendanceDetailedTable() {
    }

    public AttendanceDetailedTable(String slno, String date, String slot, String status, String units, String reason, int classnbr) {
        this.slno = slno;
        this.date = date;
        this.slot = slot;
        this.status = status;
        this.units = units;
        this.reason = reason;
        this.classnbr = classnbr;
    }

}
