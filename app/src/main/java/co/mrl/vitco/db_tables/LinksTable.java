package co.mrl.vitco.db_tables;

import com.orm.SugarRecord;

/**
 * Created by lalit on 6/9/15.
 */

public class LinksTable extends SugarRecord<LinksTable> {
    /**
     * Contains all the links from different courses.
     * In Many to one relationship
     */

    public String course_cpc, material_link, material_name;
    public boolean is_downloading;

    public LinksTable() {
    }

    public LinksTable(boolean is_downloading) {
        this.is_downloading = is_downloading;
    }

    public LinksTable(String course_cpc, String material_link, String material_name) {
        this.course_cpc = course_cpc;
        this.material_link = material_link;
        this.material_name = material_name;
        this.is_downloading = false;
    }

}
