package co.mrl.vitco.db_tables;

import com.orm.SugarRecord;

/**
 * Created by lalit on 29/8/15.
 */

public class CoursesTable extends SugarRecord<CoursesTable> {
    /**
     * Contains courses name, course code, course plan number, faculty name and course nbr
     */

    public int class_nbr;
    public String class_plan_code, course_code, course_name, faculty_name;

    public CoursesTable() {
    }

    public CoursesTable(String cpc) {
        this.class_plan_code = cpc;
    }

    public CoursesTable(int c_nbr, String course_code) {
        /**
         * Sets only class nbr ad course code
         */
        this.class_nbr = c_nbr;
        this.course_code = course_code;
    }

    public CoursesTable(String cpc, int c_nbr, String course_code, String f_name, String c_name) {
        /**
         * Sets all attributes at once.
         */
        this.class_plan_code = cpc;
        this.class_nbr = c_nbr;
        this.course_code = course_code;
        this.faculty_name = f_name;
        this.course_name = c_name;
    }


    /*
       To retrieve data from DB in bulk:
       List<CourseList> courses = CourseList.find(CourseList.class, "coursecode = ?", "2150" );

       Remove underscores from column names.

       If relation ship exits:
       List<CourseList> courses = CourseList.find(CourseList.class, "material = ?", new String{material.getId()});

       Or

       Select.from(CourseList.class).where(Condition.prop("test").eq("lalit")...);

     */


}


