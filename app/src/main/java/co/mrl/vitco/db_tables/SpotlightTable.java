package co.mrl.vitco.db_tables;

import com.orm.SugarRecord;

/**
 * Created by root on 20/10/15.
 */

public class SpotlightTable extends SugarRecord<SpotlightTable> {
    /**
     * Contains the spotlight data with row_id as their sequence number.
     */

    public int row_id;
    public String data, url;

    public SpotlightTable() {
    }

    public SpotlightTable(int row_id, String data, String url) {
        /**
         * Sets all data with its row_id
         */
        this.row_id = row_id;
        this.data = data;
        this.url = url;
    }
}
