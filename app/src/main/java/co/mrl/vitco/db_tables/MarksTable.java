package co.mrl.vitco.db_tables;

import com.orm.SugarRecord;

/**
 * Created by Lalit Umbarkar on 20/10/15.
 */
public class MarksTable extends SugarRecord<MarksTable> {

    public int class_nbr;
    public String exam_type, status, marks_scored, max_marks;

    public MarksTable() {
    }

    public MarksTable(int class_nbr, String exam_type, String status, String marks_scored, String max_marks) {
        this.class_nbr = class_nbr;
        this.exam_type = exam_type;
        this.status = status;
        this.marks_scored = marks_scored;
        this.max_marks = max_marks;
    }

}
