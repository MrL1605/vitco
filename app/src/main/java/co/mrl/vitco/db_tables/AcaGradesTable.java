package co.mrl.vitco.db_tables;

import com.orm.SugarRecord;

/**
 * Created by root on 20/10/15.
 */
public class AcaGradesTable extends SugarRecord<AcaGradesTable> {

    public String course_code, course_title, grade, credits, exam_held, result_date;

    public AcaGradesTable() {
    }

    public AcaGradesTable(String course_code, String course_title, String grade, String credits, String exam_held, String result_date) {
        this.course_code = course_code;
        this.course_title = course_title;
        this.grade = grade;
        this.credits = credits;
        this.exam_held = exam_held;
        this.result_date = result_date;
    }

}
