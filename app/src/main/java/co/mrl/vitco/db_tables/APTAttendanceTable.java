package co.mrl.vitco.db_tables;

import com.orm.SugarRecord;

/**
 * Created by root on 20/10/15.
 */
public class APTAttendanceTable extends SugarRecord<APTAttendanceTable> {

    public String date, session, status, units;

    public APTAttendanceTable() {
    }

    public APTAttendanceTable(String date, String session, String status, String units) {
        this.date = date;
        this.session = session;
        this.status = status;
        this.units = units;
    }

}
