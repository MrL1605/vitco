package co.mrl.vitco.db_tables;

import com.orm.SugarRecord;

/**
 * Created by Lalit Umbarkar on 22/10/15.
 */
public class TimeTableTable extends SugarRecord<TimeTableTable> {

    public String day, slot, room_no, course_type, course_code;

    public TimeTableTable() {
    }

    public TimeTableTable(String day, String slot, String room_no, String course_type, String course_code) {
        this.day = day;
        this.slot = slot;
        this.room_no = room_no;
        this.course_type = course_type;
        this.course_code = course_code;
    }

}
