package co.mrl.vitco.db_tables;

import com.orm.SugarRecord;

/**
 * Created by Lalit Umbarkar on 21/10/15.
 */
public class ImagesTable extends SugarRecord<ImagesTable> {

    public String image_des;
    public byte[] bitmap_data;

    public ImagesTable() {
    }

    public ImagesTable(String image_des, byte[] bitmap_data) {
        this.image_des = image_des;
        this.bitmap_data = bitmap_data;
    }
}
