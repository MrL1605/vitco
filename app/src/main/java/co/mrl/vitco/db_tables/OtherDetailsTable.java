package co.mrl.vitco.db_tables;

import com.orm.SugarRecord;

/**
 * Created by root on 20/10/15.
 */
public class OtherDetailsTable extends SugarRecord<OtherDetailsTable> {
    /**
     * Detail key will contain key words like "Name", "Ph no"
     * Detail value will contain value like "Lalit", "78458.."
     * <p/>
     * This  table can be used by following classes.
     * Academics Grades (for courses registered, cgpa ...)
     * Profile Details (store student name, Address, Phone no ...)
     * Adviser Details (store adviser name, Cabin number ...)
     * APT Attendance (classes attended, classes bunked, Attendance per)
     */

    public String detail_key, detail_value;

    public OtherDetailsTable() {
    }

    public OtherDetailsTable(String detail_key, String detail_value) {
        this.detail_key = detail_key;
        this.detail_value = detail_value;
    }

}
