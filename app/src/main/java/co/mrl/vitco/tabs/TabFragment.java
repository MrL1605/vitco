package co.mrl.vitco.tabs;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.TypedValue;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import co.mrl.vitco.R;
import co.mrl.vitco.adapter.TimeTableAdapter;
import co.mrl.vitco.db_tables.CoursesTable;
import co.mrl.vitco.db_tables.TimeTableTable;

/**
 * Created by Lalit Umbarkar on 23/10/15.
 */
public class TabFragment extends ListFragment {


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setEmptyText(getResources().getString(R.string.no_class));
        TextView emptyTextView = (TextView) getListView().getEmptyView();
        emptyTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getDimension(R.dimen.no_class__text_size));


        String day = getArguments().getString("day_name");
        Map<String, String> data = new HashMap<>();
        Iterator<TimeTableTable> retrieve_day_sch = TimeTableTable
                .find(TimeTableTable.class, "day=?", day)
                .iterator();
        if (!retrieve_day_sch.hasNext()) {
            TimeTableAdapter tta = new TimeTableAdapter(getActivity(), data);
            setListAdapter(tta);
        } else {
            String[] timings = {"8 am", "9 am", "10 am", "11 am", "12 pm",
                    "1 pm", "2 pm", "3 pm", "4 pm", "5 pm", "6 pm", "7 pm", "7 pm", "8 pm"};
            int ind = 0, pos = 0;
            while (retrieve_day_sch.hasNext()) {
                TimeTableTable curr_ = retrieve_day_sch.next();
                if (curr_.course_code.equals("SEM000")) {

                    // If it is Seminar/Meeting
                    data.put("course_name-" + pos, "Seminar/Meeting");
                    data.put("time-" + pos, timings[ind]);
                    data.put("room_no-" + pos, curr_.room_no);
                    data.put("slot-" + pos, curr_.slot);
                    pos++;

                } else if (!curr_.course_code.equals("==")) {
                    //
                    // Key are => course_code-1 , room-1 where 1 is position
                    //

                    // If course name with code exists
                    List<CoursesTable> ct = CoursesTable.find(CoursesTable.class, "coursecode=?", curr_.course_code);
                    if (ct.size() >= 1)
                        data.put("course_name-" + pos, ct.get(0).course_name);
                    else
                        data.put("course_name-" + pos, " - ");
                    data.put("time-" + pos, timings[ind]);
                    data.put("room_no-" + pos, curr_.room_no);
                    data.put("slot-" + pos, curr_.slot);
                    pos++;
                }
                ind++;
            }
            TimeTableAdapter tta = new TimeTableAdapter(getActivity(), data);
            setListAdapter(tta);
        }
    }

    public TabFragment() {
    }

}
