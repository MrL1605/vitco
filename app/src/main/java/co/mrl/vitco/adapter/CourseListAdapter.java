package co.mrl.vitco.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import co.mrl.vitco.R;
import co.mrl.vitco.activity.LinkListActivity;

/**
 * Created by lalit on 5/9/15.
 */

@SuppressWarnings("deprecation")
public class CourseListAdapter extends ArrayAdapter<String> {

    String[] course_names, course_faculty, course_codes, class_nbr;
    LayoutInflater inflater;
    Activity context;

    public CourseListAdapter(Activity context, String[] c_names, String[] c_codes, String[] c_faculty, String[] nbr) {

        super(context, R.layout.course_list_item, c_names);
        this.context = context;
        this.course_names = c_names;
        this.course_codes = c_codes;
        this.course_faculty = c_faculty;
        this.class_nbr = nbr;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int i, final View view, ViewGroup viewGroup) {

        Holder hold = new Holder();

        View rowView;
        rowView = inflater.inflate(R.layout.course_list_item, null);
        hold.course_name = (TextView) rowView.findViewById(R.id.course_name_tv);
        hold.course_code = (TextView) rowView.findViewById(R.id.course_code_tv);
        hold.course_faculty = (TextView) rowView.findViewById(R.id.course_faculty_tv);
        hold.course_name.setText(course_names[i]);
        hold.course_faculty.setText(course_faculty[i]);
        hold.course_code.setText(course_codes[i]);
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, LinkListActivity.class);
                intent.putExtra("class_nbr", class_nbr[i]);
                context.startActivity(intent);
            }
        });
        return rowView;
    }

    public class Holder {
        TextView course_name, course_code, course_faculty;
    }
}
