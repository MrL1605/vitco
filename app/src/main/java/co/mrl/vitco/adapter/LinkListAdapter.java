package co.mrl.vitco.adapter;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import co.mrl.vitco.R;
import co.mrl.vitco.db_tables.CoursesTable;
import co.mrl.vitco.db_tables.LinksTable;

/**
 * Created by lalit on 6/9/15.
 */

@SuppressWarnings("deprecation")
public class LinkListAdapter extends ArrayAdapter<String> {

    public static String PATH_NAME = "/VITco/FALL2015/";
    String[] material_names, material_url;
    boolean[] is_downloading;
    String curr_cpc;
    LayoutInflater inflater;
    Activity context;

    public LinkListAdapter(Activity context, String cpc, String[] m_name, String[] m_url, boolean[] is_downloading) {

        super(context, R.layout.course_list_item, m_name);
        this.context = context;
        this.material_names = m_name;
        this.material_url = m_url;
        this.curr_cpc = cpc;
        this.is_downloading = is_downloading;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int i, final View view, ViewGroup viewGroup) {

        final Holder hold = new Holder();
        final View rowView = inflater.inflate(R.layout.link_list_item, null);
        hold.material_name = (TextView) rowView.findViewById(R.id.material_name_tv);
        hold.status_iv = (ImageView) rowView.findViewById(R.id.status_img);
        hold.download_p_bar = (ProgressBar) rowView.findViewById(R.id.download_p_bar);

        hold.material_name.setText(material_names[i]);

        //Check if file is downloading or not
        if (!is_downloading[i]) {
            if (file_exists(i)) {
                hold.status_iv.setImageResource(R.drawable.black_tick30);
            } else {
                hold.status_iv.setImageResource(R.drawable.down_arrow_30);
            }
        } else {
            hold.status_iv.setVisibility(View.GONE);
            hold.download_p_bar.setVisibility(View.VISIBLE);
        }

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (is_downloading[i]) {
                    Toast.makeText(context, "Downloading...", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<CoursesTable> ct = CoursesTable.find(CoursesTable.class, "classplancode = ?", curr_cpc);
                String course_name = ct.get(0).course_name;
                if (file_exists(i)) {
                    // Open file
                    File root = android.os.Environment.getExternalStorageDirectory();
                    String tmp_file_path = root.getAbsolutePath() + (PATH_NAME + course_name + "/" + material_names[i]);

                    String fileType;
                    fileType = tmp_file_path.substring(tmp_file_path.indexOf('.', tmp_file_path.lastIndexOf('/')));
                    fileType = fileType.substring(1);

                    switch (fileType) {
                        case "pdf":
                            fileType = "pdf";
                            break;
                        case "ppt":
                        case "pptx":
                            fileType = "vnd.ms-powerpoint";
                            break;
                        case "doc":
                        case "docx":
                            fileType = "msword";
                            break;
                        default:
                            fileType = "*";
                            break;
                    }

                    File curr_file = new File(tmp_file_path);
                    Intent viewDoc = new Intent(Intent.ACTION_VIEW);
                    viewDoc.setDataAndType(
                            Uri.fromFile(curr_file),
                            "application/" + fileType);

                    PackageManager pm = context.getPackageManager();
                    List<ResolveInfo> apps =
                            pm.queryIntentActivities(viewDoc, PackageManager.MATCH_DEFAULT_ONLY);

                    if (apps.size() > 0)
                        context.startActivity(viewDoc);
                    else
                        Toast.makeText(context, "File Type Not supported yet", Toast.LENGTH_LONG).show();
                } else {

                    is_downloading[i] = true;
                    hold.status_iv.setVisibility(View.GONE);
                    hold.download_p_bar.setVisibility(View.VISIBLE);
                    String url = material_url[i];
                    String file_name = material_names[i];
                    final String file_path_name = PATH_NAME + course_name;

                    class DownloadMaterial extends AsyncTask<String, String, String> {

                        String path_name, file_name, download_url;
                        CountDownTimer cdt;
                        int id = i;
                        NotificationManager mNotifyManager;
                        NotificationCompat.Builder mBuilder;

                        @Override
                        protected void onPreExecute() {
                            /**
                             * Create custom Count Down Timer
                             * In case certificate are Expired.
                             */
                            setTrustAllCerts();
                            cdt = new CountDownTimer(100 * 60 * 1000, 500) {
                                public void onTick(long millisUntilFinished) {
                                    mNotifyManager.notify(id, mBuilder.build());
                                }

                                public void onFinish() {
                                    mNotifyManager.notify(id, mBuilder.build());
                                }
                            };
                        }

                        @Override
                        protected String doInBackground(String... strings) {

                            download_url = strings[0];
                            file_name = strings[1];
                            path_name = strings[2];
                            /**
                             * Start timer to update Notification
                             * Set Progress to 20 after connection
                             * Build Notification
                             * Increment Progress
                             * Download and Save file
                             */
                            try {
                                mNotifyManager =
                                        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                                mBuilder = new NotificationCompat.Builder(context);
                                mBuilder.setContentTitle("Downloading File")
                                        .setContentText(file_name)
                                        .setProgress(0, 100, false)
                                        .setOngoing(true)
                                        .setPriority(Notification.PRIORITY_LOW)
                                        .setSmallIcon(R.mipmap.vitco_logo);
                                File root = android.os.Environment.getExternalStorageDirectory();
                                File dir = new File(root.getAbsolutePath() + path_name);
                                if (!dir.exists()) {
                                    dir.mkdirs();
                                }
                                publishProgress("5");
                                mNotifyManager.notify(id, mBuilder.build());
                                cdt.start();

                                URL url = new URL(download_url);
                                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                                connection.connect();

                                // expect HTTP 200 OK, so we don't mistakenly save error report
                                // instead of the file
                                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                                    throw new Exception();
                                }

                                // this will be useful to display download percentage
                                // might be -1: server did not report the length
                                int fileLength = connection.getContentLength();

                                // download the file
                                InputStream input = connection.getInputStream();
                                OutputStream output = new FileOutputStream(root.getAbsolutePath() + path_name + "/" + file_name);

                                publishProgress("20");
                                byte data[] = new byte[4096];
                                long total = 0;
                                int count;
                                while ((count = input.read(data)) != -1) {
                                    total += count;
                                    if (fileLength > 0) {
                                        publishProgress("" + (int) (20 + (total * 80 / fileLength)));
                                    }
                                    output.write(data, 0, count);
                                }
                                mBuilder.setContentTitle("Downloaded")
                                        .setContentInfo("")
                                        .setOngoing(false)
                                        .setProgress(0, 0, false);
                                cdt.onFinish();
                                cdt.cancel();
                                output.close();
                                input.close();
                                connection.disconnect();

                            } catch (Exception e) {
                                return "Failed";
                            }
                            return "Success";
                        }

                        @Override
                        protected void onProgressUpdate(String... values) {
                            /**
                             * Update Download Progress
                             */
                            mBuilder.setContentInfo(values[0] + "%")
                                    .setProgress(100, Integer.parseInt(values[0]), false);
                        }

                        @Override
                        protected void onPostExecute(String s) {

                            if (s.equals("Success")) {
                                hold.status_iv.setImageResource(R.drawable.black_tick30);
                                Toast.makeText(context.getApplication(), "File Downloaded as " + file_name, Toast.LENGTH_LONG).show();
                            } else {
                                mBuilder.setContentTitle("Error Occurred\t   ¯\\_(ツ)_/¯")
                                        .setContentInfo("")
                                        .setOngoing(false)
                                        .setProgress(0, 0, false);
                                cdt.onFinish();
                                cdt.cancel();
                                hold.status_iv.setImageResource(R.drawable.down_arrow_30);
                                Toast.makeText(context.getApplication(), "Error Occurred. Try Again", Toast.LENGTH_SHORT).show();
                            }
                            is_downloading[i] = false;
                            hold.download_p_bar.setVisibility(View.GONE);
                            hold.status_iv.setVisibility(View.VISIBLE);
                            List<LinksTable> down_modify = LinksTable.find(LinksTable.class, "materiallink = ?", material_url[i]);
                            LinksTable tmp_lt = down_modify.get(0);
                            tmp_lt.is_downloading = false;
                            tmp_lt.save();

                        }
                    }
                    new DownloadMaterial().execute(url, file_name, file_path_name);
                }
            }
        });
        return rowView;
    }

    public boolean file_exists(int index) {

        List<CoursesTable> ct = CoursesTable.find(CoursesTable.class, "classplancode = ?", curr_cpc);
        String course_name = ct.get(0).course_name;
        File root = android.os.Environment.getExternalStorageDirectory();
        File f = new File(root.getAbsolutePath() + PATH_NAME + course_name, material_names[index]);
        return f.exists() && !f.isDirectory();
    }

    public void setTrustAllCerts() {
        /**
         * To Bypass SSL Certificate Check. In case the Academics Portal Certificate is expired.
         */
        try {
            // Create a custom Trust Manager
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };


            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                        public boolean verify(String urlHostName, SSLSession session) {
                            return true;
                        }
                    });
        } catch (Exception e) {
            //We can not recover from this exception.
            //e.printStackTrace();
        }
    }

    public class Holder {
        TextView material_name;
        ImageView status_iv;
        ProgressBar download_p_bar;
    }


}


