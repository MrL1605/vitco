package co.mrl.vitco.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lalit Umbarkar on 23/10/15.
 */
public class PageAdapter extends FragmentPagerAdapter {

    private final List<Fragment> fragment_list = new ArrayList<>();
    private final List<String> fragment_title_list = new ArrayList<>();

    public PageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragment_list.get(position);
    }

    @Override
    public int getCount() {
        return fragment_list.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragment_title_list.get(position);
    }

    public void add_frag(Fragment tab_frag, String title){
        fragment_list.add(tab_frag);
        fragment_title_list.add(title);
    }

}
