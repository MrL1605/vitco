package co.mrl.vitco.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import co.mrl.vitco.R;

/**
 * Created by Lalit Umbarkar on 24/10/15.
 */
public class MarksListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> courses_list;
    private Map<String,String> marks_map;

    public MarksListAdapter(Context context, List<String> courses_list, Map<String,String> marks_map) {
        this.context = context;
        this.courses_list = courses_list;
        this.marks_map = marks_map;
    }

    @Override
    public int getGroupCount() {
        return courses_list.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return courses_list.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.marks_map.get(this.courses_list.get(groupPosition));
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.marks_list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.course_name_tv);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        try {
            LinearLayout layout = new LinearLayout(this.context);
            layout.setOrientation(LinearLayout.VERTICAL);
            String marks = (String)getChild(groupPosition, childPosition);
            for(String exam : marks.split("#")){

                TextView tv = new TextView(this.context);
                String data = "";
                String[] exam_detail = exam.split("&");
                data += exam_detail[0].split("=")[1] + " : " +
                        exam_detail[1].split("=")[1] + "/" + exam_detail[2].split("=")[1];
                tv.setText(data);
                layout.addView(tv);
            }
            return layout;
        }catch (Exception ignore){}
        return null;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
