package co.mrl.vitco.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Map;

import co.mrl.vitco.R;

/**
 * Created by Lalit Umbarkar on 24/10/15.
 */
public class TimeTableAdapter extends ArrayAdapter<String> {

    Map<String,String> schedule;
    LayoutInflater inflater;

    public TimeTableAdapter(Context context, Map<String,String> sch) {
        super(context, R.layout.schedule_list_item);
        this.schedule = sch;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        Holder hold = new Holder();
        View row_view;
        row_view = inflater.inflate(R.layout.schedule_list_item, null);

        hold.slot_tv = (TextView)row_view.findViewById(R.id.slot_tv);
        hold.time_tv = (TextView)row_view.findViewById(R.id.time_tv);
        hold.course_name_tv = (TextView)row_view.findViewById(R.id.course_name_tv);
        hold.room_tv = (TextView)row_view.findViewById(R.id.room_no_tv);

        hold.slot_tv.setText(schedule.get("slot-" + position));
        hold.time_tv.setText(schedule.get("time-" + position));
        hold.course_name_tv.setText(schedule.get("course_name-" + position));
        hold.room_tv.setText(schedule.get("room_no-" + position));
        return row_view;
    }

    @Override
    public int getCount() {
        return (schedule.size()/4);
    }

    public class Holder{
        TextView course_name_tv;
        TextView time_tv;
        TextView room_tv;
        TextView slot_tv;
    }

}
