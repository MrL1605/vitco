package co.mrl.vitco.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import co.mrl.vitco.R;
import co.mrl.vitco.activity.AttendanceDetailActivity;

/**
 * Created by Lalit Umbarkar on 25/10/15.
 */
public class AttendanceListAdapter extends ArrayAdapter<String> {

    String[] course_names, attend_summary, course_codes, class_nbr;
    LayoutInflater inflater;
    Activity context;

    public AttendanceListAdapter(Activity context, String[] c_names, String[] c_codes, String[] c_attend, String[] nbr) {
        super(context, R.layout.course_list_item);
        this.context = context;
        this.course_names = c_names;
        this.course_codes = c_codes;
        this.attend_summary = c_attend;
        this.class_nbr = nbr;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder hold = new Holder();

        View rowView;
        rowView = inflater.inflate(R.layout.course_list_item, null);
        hold.course_name = (TextView) rowView.findViewById(R.id.course_name_tv);
        hold.course_code = (TextView) rowView.findViewById(R.id.course_code_tv);
        hold.att_summary = (TextView) rowView.findViewById(R.id.course_faculty_tv);
        hold.course_name.setText(course_names[position]);
        Log.i("Attend val","" + attend_summary[position]);
        hold.att_summary.setText(attend_summary[position]);
        hold.course_code.setText(course_codes[position]);
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, AttendanceDetailActivity.class);
                intent.putExtra("class_nbr", class_nbr[position]);
                context.startActivity(intent);
            }
        });
        return rowView;

    }

    @Override
    public int getCount() {
        return course_names.length;
    }

    public class Holder {
        TextView course_name, course_code, att_summary;
    }

}
