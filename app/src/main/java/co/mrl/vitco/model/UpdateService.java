package co.mrl.vitco.model;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import co.mrl.vitco.R;
import co.mrl.vitco.db_tables.CoursesTable;
import co.mrl.vitco.db_tables.LinksTable;
import co.mrl.vitco.db_tables.UpdatedLinksTable;

/**
 * Created by lalit on 8/9/15.
 */

@SuppressWarnings("deprecation")
public class UpdateService extends IntentService {

    public static final String ACCESS_URL = "https://academics.vit.ac.in/student/stud_login.asp";
    public static final String SUBMIT_URL = "https://academics.vit.ac.in/student/stud_login_submit.asp";
    public static final String CAPTCHA_URL = "https://academics.vit.ac.in/student/captcha.asp";
    public static final String COURSE_PAGE_URL = "https://academics.vit.ac.in/student/coursepage_view3.asp";
    Map<String, String> cookies;
    Map<String, String> formFields;

    public UpdateService(String name) {
        super(name);
    }

    public UpdateService() {
        super("UpdateService");
    }

    @Override
    protected void onHandleIntent(Intent in) {
        /**
         * At specific interval of time this service is called to update database of VITco in background.
         */

        // In case certificate are Expired.
        setTrustAllCerts();

        // Get if Alarm is set or not.
        SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
        String alarm_set = sp.getString("alarm_set", "");

        if (!(alarm_set.equals("") || alarm_set.equals("0"))) {

            try {
                // Do Login
                if (!academics_login().equals("Success")) {
                    return;
                }

                // Get all new Data from Academics in UpdatedLinksTable
                if (!all_course_page_retriever().equals("Success")) {
                    return;
                }

                // Compare LinksTable(old data) and UpdatedLinksTable(New Data)
                if (compare_update()) {

                    // Send Notification to user about updating of data
                    NotificationManager NManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(UpdateService.this)
                                    .setSmallIcon(R.mipmap.vitco_logo)
                                    .setContentTitle("VITco")
                                    .setContentText("New Files Added to Course Page");
                    NManager.notify(2, mBuilder.build());
                }

            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
    }

    public String academics_login() {
        /**
         * Returns  "Success" -> Login successfully.
         *          "Failed" -> UpdatedLinksTable could not be updated successfully.
         *          "Wrong Password" -> When user's Reg No. and Passwords do not match.
         * Does Login for academics.
         * Reg No and Password extracted from Shared Preferences of "VITco".
         * Stores cookies returned in cookies variable.
         */

        Connection.Response resp;
        String cap_val;
        try {
            resp = Jsoup.connect(ACCESS_URL)
                    .timeout(300000)
                    .method(Connection.Method.GET)
                    .userAgent("Mozilla/5.0").execute();

            // Get request to login form of Academics Page, and store cookies, as well as their form fields.
            // Cookies are important. We will use them everywhere.

            cookies = resp.cookies();

            Elements fields = resp.parse().select("form input");
            formFields = new HashMap<>();
            for (Element field : fields) {
                if (!field.attr("name").equals("")) {
                    formFields.put(field.attr("name"), field.attr("value"));
                }
            }

            // Post request to Captcha page to retrieve Captcha Image, using cookies.

            Connection.Response resultImageResponse = Jsoup.connect(CAPTCHA_URL)
                    .cookies(cookies)
                    .ignoreContentType(true)
                    .method(Connection.Method.GET).timeout(30000).execute();


            byte[] bitmap_Data = resultImageResponse.bodyAsBytes();
            Bitmap bm = BitmapFactory.decodeByteArray(bitmap_Data, 0, bitmap_Data.length);

            // Send this Bitmap to CaptchaParser.java to parse it,
            // It returns captcha value as XXXXXX.

            CaptchaParser cp = new CaptchaParser();
            cp.setBm(bm);
            cap_val = cp.getCaptcha();

            // Get Reg No and Password and make login Login using Post request with cookies.

            SharedPreferences sp = getSharedPreferences("VITco Prefs", MODE_PRIVATE);
            String reg_no = sp.getString("reg_no", "");
            String password = sp.getString("password", "");

            if (!(reg_no.equals("") || password.equals(""))) {
                formFields.put("regno", reg_no);
                formFields.put("passwd", password);
                formFields.put("vrfcd", cap_val);
            } else {
                throw new WrongPassword();
            }

            Connection.Response access_url_resp = Jsoup.connect(SUBMIT_URL)
                    .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            // Extra headers
                    .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                    .header("Accept-Encoding", "gzip, deflate")
                    .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                    .header("Host", "academics.vit.ac.in")
                    .header("Connection", "keep-alive")
                    .header("DNT", "1")
                    .header("Referer", "https://academics.vit.ac.in/student/stud_login.asp")
                    .cookies(cookies)
                    .followRedirects(true)
                    .data(formFields)
                    .timeout(300000)
                    .method(Connection.Method.POST).execute();

            // Check if logged in by checking if cookies returned has reg number in it.
            // If Reg No was there, then Academics server accepted Login. Otherwise throw Exception.

            boolean logged_in = false;
            Map<String, String> temp = access_url_resp.cookies();
            for (String key : temp.keySet()) {
                if (key.equals("logstudregno")) {
                    if (temp.get(key).equals(reg_no)) {
                        logged_in = true;
                    }
                }
                cookies.put(key, temp.get(key));
            }

            if (!logged_in) {
                throw new WrongPassword();
            }

        } catch (WrongPassword wp) {
            // Password Wrong Caught here.
            //wp.printStackTrace();
            return "Wrong Password";
        } catch (Exception e) {
            // Any Connection Error Caught here.
            //e.printStackTrace();
            return "Failed";
        }

        // Next step, Retrieve all data from all courses.
        all_course_page_retriever();
        return "Success";

    }

    public String all_course_page_retriever() {
        /**
         * Returns  "Success" -> UpdatedLinksTable updated with new links successfully.
         *          "Failed" -> UpdatedLinksTable could not be updated successfully.
         * Retrieves all course page links from all courses available in CoursesTable.
         * Note :- CoursesTable is not updated.
         */

        // Delete all last updated data, and get all courses

        UpdatedLinksTable.deleteAll(UpdatedLinksTable.class);
        Iterator<CoursesTable> iter_ct = CoursesTable.findAll(CoursesTable.class);

        // Iterate through all cpc and get Links.

        while (iter_ct.hasNext()) {
            CoursesTable curr_ct = iter_ct.next();
            String curr_cpc = curr_ct.class_plan_code;

            try {
                formFields.clear();
                formFields.put("sem", "FS");
                formFields.put("crsplancode", curr_cpc);
                formFields.put("crpnvwcmd", "View");

                Connection.Response course_page_resp = Jsoup.connect(COURSE_PAGE_URL)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                        .header("Connection", "keep-alive")
                        .header("Host", "academics.vit.ac.in")
                        .header("Referer", "https://academics.vit.ac.in/student/coursepage_view.asp?sem=FS&crs=&slt=&fac=")
                        .header("DNT", "1")
                        .cookies(cookies)
                        .data(formFields)
                        .ignoreContentType(false)
                        .timeout(300000).followRedirects(true)
                        .method(Connection.Method.POST).execute();

                // At first redirected to Spotlight page. Second try to get Course Page

                if (course_page_resp.statusCode() == 200) {
                    course_page_resp = Jsoup.connect(COURSE_PAGE_URL)
                            .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0")
                            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                            .header("Accept-Encoding", "gzip, deflate")
                            .header("Accept-Language", "en-US,en;q=0.7,mr;q=0.3")
                            .header("Connection", "keep-alive")
                            .header("Host", "academics.vit.ac.in")
                            .header("Referer", "https://academics.vit.ac.in/student/coursepage_view.asp?sem=FS&crs=&slt=&fac=")
                            .header("DNT", "1")
                            .cookies(cookies)
                            .data(formFields)
                            .ignoreContentType(false)
                            .timeout(300000).followRedirects(true)
                            .method(Connection.Method.POST).execute();
                }

                // Get links and extract material name from it.

                Document doc = course_page_resp.parse();
                Elements links = doc.select("a[target=\"_blank\"]");
                String material_name = "";
                for (Element link : links) {
                    String material_url = link.attr("href");
                    try {
                        if (material_url.split("[A-Z]{2,4}[0-9]{2}_").length >= 2) {
                            String[] tmp = material_url.split("[A-Z]{2,4}[0-9]{2}_");
                            material_name = tmp[tmp.length - 1];
                        } else {
                            material_name = material_url.split(curr_cpc)[1];
                        }
                    } catch (ArrayIndexOutOfBoundsException aiob) {
                        //aiob.printStackTrace();
                    }
                    if (!(material_name.equals("") || material_url.equals(""))) {
                        //Store it in UpdatedLinksTable instead to LinksTable to compare later.

                        UpdatedLinksTable add_new = new UpdatedLinksTable(curr_cpc, material_url, material_name);
                        add_new.save();
                    }
                }
            } catch (Exception e) {
                // Any Connection Error Caught here.
                //e.printStackTrace();
                return "Failed";
            }
        }
        return "Success";
    }

    public boolean compare_update() {
        /**
         * Returns  true -> New updates found.
         *          false-> No new updates found.
         * Used to compare the UpdatedLinksTable and the LinksTable, to check if any news updates are made.
         */

        int i = 0;
        List<UpdatedLinksTable> ult_list = UpdatedLinksTable.findWithQuery(UpdatedLinksTable.class, "Select * from updated_links_table");
        List<LinksTable> links_list = LinksTable.findWithQuery(LinksTable.class, "Select * from links_table");
        int ult_size = ult_list.size();
        int links_size = links_list.size();

        // Check if more data is found. Than we had before.

        if (ult_size > links_size) {

            // Delete whole old data. We got new data to store in it.
            LinksTable.deleteAll(LinksTable.class);
            while (i < ult_size) {
                UpdatedLinksTable curr_ult = ult_list.get(i);
                LinksTable lt = new LinksTable(curr_ult.course_cpc, curr_ult.material_link, curr_ult.material_name);
                lt.save();
                i++;
            }
            return true;

        } else {
            return false;
        }
    }

    public void setTrustAllCerts() {
        /**
         * To Bypass SSL Certificate Check. In case the Academics Portal Certificate is expired.
         */
        try {
            // Create a custom Trust Manager
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };


            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                        public boolean verify(String urlHostName, SSLSession session) {
                            return true;
                        }
                    });
        } catch (Exception e) {
            //We can not recover from this exception.
            //e.printStackTrace();
        }
    }

    public class WrongPassword extends Exception {
        /**
         * Wrong Password exception thrown when, uer enters wrong password.
         */
        public WrongPassword() {
        }

        @Override
        public String toString() {
            return "Wrong Password Entered";
        }
    }


}