VITco
==================
Course Page of VIT Academics on your phone

### Installation
VITco is published on [GetJar](http://bit.ly/vitco_getjar)

Direct Download Signed Apk [here](http://bit.ly/mrl_vitco_apk)

### Description
Now get Academics Course Page updates on your phone.
Single click Downloads and view of Materials.

> Note:  **Passwords are not stored anywhere except your phone.**

There is no way your Academics password would be leaked. Unless an Android Hacker steals your phone and hacks his way to get access to PRIVATE mode of Shared Prefrences of VITco App. And like that's gonna happen anytime :D

###### Features:
- Auto Login to Academics
- Auto Files renamed according to file name on Course Page.
- Stored in appropriate Folder Name as Course Name.
- Notified Updates of Course Page for your slot.
- Automatic update Course List in Background. (Turn it off if you don't want it)
- Open downloaded files from app itself. Supported file types :- pdf, doc, docx, ppt, pptx.

###### Future Updates:
- Support for all semesters (Currently works only for FALL SEM)
- Support for all media files. (Some faculties upload videos and images for students)
- Improved UI

Special Thanks to [Karthik Balakrishnan's Captcha Parser](https://github.com/karthikb351/CaptchaParser) which made VITco automated.

### Contact
Drop an Email at : lalit.umbarkar9@gmail.com

Or create an [issue here](https://github.com/MrL1605/VITco/issues)

### License
MIT 

### Version 
3.5.8

